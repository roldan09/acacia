# Acacia

Código fuente del sistema de recomendaciones.

# Créditos

Resultado del proyecto ACACIA (561754-EPP-1-2015-1-CO-EPPKA2-CBHE-JP) 
cofinanciado por el programa Erasmus+ ACACIA: Centros de Cooperación para el 
Fomento, Fortalecimiento y Transferencia de Buenas Prácticas que Apoyan, 
Cultivan, Adaptan, Comunican, Innovan y Acogen a la comunidad universitaria. 

# Agradecimientos

Esta obra ha sido posible gracias al compromiso de todos los miembros del 
consorcio del proyecto ACACIA (Centros de Cooperación para el Fomento, 
Fortalecimiento y Transferencia de Buenas Prácticas que Apoyan, Cultivan, 
Adaptan, Comunican, Innovan y Acogen a la comunidad universitaria.) y a la 
cofinanciación recibida por parte de la Comisión Europea a través del Programa
Erasmus+. 

# Descripción

- Dev: Comprende los códigos fuentes de los componentes de la
arquitectura de software.

- Db: Contiene los scripts de consulta a repositorio de datos y
archivos de resultados de procesamiento de datos.

- Frontend: Contiene los códigos fuente del componente de
Frontend encargado de capturar las imágenes del los host
de los usuarios estudiantes a través del script que actúa
como un pluguin en la plataforma EDX.

- Processing: Contiene los códigos fuente del componente de
Processing el cual comprende la máquina de aprendizaje
automático.

- Training: Contiene los códigos fuente del componente de
Training el cual captura la información de retroalimentación
por parte de los usuarios Profesores, a su vez les muestra
los reportes de desempeño de los estudiantes de un curso.

- Vision: Contiene los códigos fuente del componente de
Vision el cual a partir de Affectiva obtiene un vector de
características de las imágenes capturadas por el
componente Frontend.

- Dist: Comprende el script de despliegue en el servidor de
producción del sista.

- Doc: Documentos de informe como el presente documento.
