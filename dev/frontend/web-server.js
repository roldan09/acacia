/** 
 * =========================================================
 * = Universidad Distrital Francisco Jose de Caldas        =
 * = Project: ACACIA                                       =
 * = @ All rights reserved. April 2019.                    =
 * = Autor: Ing. Fabian Jose Roldan Piñeros.               =
 * = Version: 1.0                                          =
 * =========================================================
*/

/**
 * Component that represent the end point and presentation 
 * of acacia recommender system.
 */

//-----------------------------------------------------------
// Define application parameters.
//-----------------------------------------------------------
const WEB_SERVER_LISTEN_PORT = 7000;
const VISION_COMPONENT_HOST = 'acacia-app_vision_1';
const VISION_COMPONENT_PORT = 7002;
const DEPLOY_MODE = false;

//-----------------------------------------------------------
// Define of the required node js components.
//-----------------------------------------------------------
var express = require('express'),
    path = require('path'),
    formidable = require('formidable'),
    readChunk = require('read-chunk'),
    fileType = require('file-type');
var https = require('https');
var cors = require('cors');
var app = express();
var fs = require('fs');
var sleep = require('system-sleep');
var bodyParser = require("body-parser");

//-----------------------------------------------------------
// Starting log.
//-----------------------------------------------------------
console.log('');
console.log('');
console.log('***********************************************');
console.log('***********************************************');
console.log('');
console.log('Date: ' + getDate());
console.log('Web server starting...');
console.log('');

//-----------------------------------------------------------
// Define of the web server component.
//-----------------------------------------------------------
https.createServer({
  key: fs.readFileSync('/usr/src/app/conf/server.key'),
  cert: fs.readFileSync('/usr/src/app/conf/server.crt'),
  passphrase: 'distrital'
}, app).listen(WEB_SERVER_LISTEN_PORT, function () {
  console.log('Web server listening on port ' + WEB_SERVER_LISTEN_PORT + '.');
})

// Cors
app.options('*', cors());

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

// Tell express to serve static files from the following 
// directories
app.use(express.static('public'));
app.use('/uploads', express.static('uploads'));

// Here we are configuring express to use body-parser 
// as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//-----------------------------------------------------------
// Define of the database manager.
//-----------------------------------------------------------
const pgClient = require('pg').Client;
const client = new pgClient({
  user: 'postgres',
  host: 'acacia-app_db_1',
  database: 'baxterdb',
  password: 'example',
  port: 5432,
});
client.connect();

//-----------------------------------------------------------
// Define of connection with the vision component.
//-----------------------------------------------------------
var attemps = 0;
var net = require('net');
var skt = new net.Socket();
var visionConnectionAlive = false;

// Start the connection.
console.log('Init conection...');
skt.connect(VISION_COMPONENT_PORT, VISION_COMPONENT_HOST, function() {
    console.log('Connected');
    console.log('Conection with vision [OK].');
    visionConnectionAlive = true;
});

// Handler of responses from vision component.
skt.on('data', function(data) {
    console.log('Received: ' + data);
});

// Handler of the close of connection.
skt.on('close', function() {
    // Log.
    console.log('Connection closed.');
    visionConnectionAlive = false;
    // Delay
    sleep(1000);
    // Reconnect.
    console.log('Re-connecting to vision component...');
    reconnect();   
});

//-----------------------------------------------------------
// Define the web server methods.
//-----------------------------------------------------------

/**
 * Handler all errors of the system.
 */
process.on('uncaughtException', function (err) {
    console.log(err);
});

/**
 * Index route
 */
app.get('/', function (req, res) {
    // Response with index view.
    res.sendFile(path.join(__dirname, 'views/index.html'));
});

/**
 * Controller route
 */
app.get('/controller', function (req, res) {
    // Response with index view.
    res.sendFile(path.join(__dirname, 'views/controller.html'));
});

/**
 * Set authorization.
 */
app.post('/set_authorization', function (req, res) {
    // Define variables
    var query;
    var response = 200;
    
    // Get parametres.
    var userID = req.body.user_id;
    var userName = req.body.user_name;
    var courseID = req.body.course_id;
    var courseName = req.body.course_name;
    var val = req.body.val;

    // Get current date
    var date = getDate();

    // Log.
    console.log("Checking user id:", userID);

    // Ckeck if user exist.
    query = "select user_name from public.baxter_usertable where user_id = '" + userID + "'";
    client.query(query, (err, res) => {
        if(err) {
            console.log('ERROR select user_name from public.baxter_usertable where user_id:');
            console.log(err);
            response = 500;      
        } else {
            if (res.rows.length <= 0) {
                // Create a user.
                query = "insert into public.baxter_usertable(user_id, user_name, updated) values('" + userID + "','" + userName + "','" + date +"')";        
                client.query(query, (err2, res2) => {
                    if(err2) {
                        console.log('ERROR insert into public.baxter_usertable(user_id, user_name, updated) values:');
                        console.log(err2);
                        response = 500;      
                    } else {
                        console.log("User created with:", userID);
                    }
                });
            }
            console.log("User [OK]");
        }
    });

    // Log.
    console.log("Checking course id:", courseID);

    // Ckeck if course exist.
    if (response != 500) {
        query = "select course_name from public.baxter_coursetable where course_id = '" + courseID + "'";
        client.query(query, (err, res) => {
            if(err) {
                console.log('ERROR select course_name from public.baxter_coursetable:');
                console.log(err);
                response = 500;      
            } else {
                if (res.rows.length <= 0) {
                    // Create a course.
                    query = "insert into public.baxter_coursetable(course_id, course_name, updated) values('" + courseID + "','" + courseName + "','" + date +"')";        
                    client.query(query, (err2, res2) => {
                        if(err2) {
                            console.log('ERROR insert into public.baxter_coursetable:');
                            console.log(err2);
                            response = 500;      
                        } else {
                            console.log("Course created with ID:", courseID);
                        }
                    });
                }
                console.log("Course [OK]");
            }
        });
    }

    // Update a StudentXCourseTable.
    // Ckeck if table exist.
    if (response != 500) {
        query = "select student_id from public.baxter_studentxcoursetable where student_id = '" + userID + "' and course_id = '" + courseID  + "'";
        client.query(query, (err, res) => {
            if(err) {
                console.log('ERROR select student from public.baxter_studentxcoursetable:');
                console.log(err);
                response = 500;      
            } else {
                if (res.rows.length <= 0) {
                    // Create a studentxcoursetable.
                    query = "insert into public.baxter_studentxcoursetable values (DEFAULT," + val + " ,'" + courseID + "','" + userID + "')";        
                    client.query(query, (err2, res2) => {
                        if(err2) {
                            console.log('ERROR insert into public.baxter_studentxcoursetable:');
                            console.log(err2);
                            response = 500;      
                        } else {
                            console.log("Student x Course Table created");
                        }
                    });
                } else {
                    query = "update public.baxter_studentxcoursetable set auth = " + val + " where student_id = '" + userID + "' and course_id = '" + courseID  + "'";        
                    client.query(query, (err2, res2) => {
                        if(err2) {
                            console.log('ERROR update public.baxter_studentxcoursetable:');
                            console.log(err2);
                            response = 500;      
                        } else {
                            console.log("Set user authorization with:", val);
                        }
                    });
                }                
            }
        });
    }

    // Return response.
    res.status(response);
});

/**
 * Upload photos method.
 */
app.post('/upload_photos', function (req, res) {

    // Define de user id variable.
    var userID = null;

    // Define of upload object. 
    var form = new formidable.IncomingForm();

    // Tells formidable that there will be multiple files sent.
    form.multiples = true;
    
    // Upload directory for the images
    form.uploadDir = path.join(__dirname, 'tmp_uploads');

    // Invoked when a file has finished uploading.
    form.on('file', function (name, file) {

        var buffer = null,
            type = null,
            filename = '';

        console.log('Read a chunk of the file.');    
        // Read a chunk of the file.
        buffer = readChunk.sync(file.path, 0, 262);
        
        console.log('Get the file type using the buffer read using read-chunk');
        // Get the file type using the buffer read using read-chunk
        type = fileType(buffer);

        // Check the file type, must be either png,jpg or jpeg
        if (type !== null && (type.ext === 'png' || type.ext === 'jpg' || type.ext === 'jpeg')) {
            
            console.log('Assign new file name.')
            // Assign new file name
            filename = Date.now() + '-' + file.name;

            console.log('Move the file with the new file name from: ' + file.path);
            // Move the file with the new file name
            fs.rename(file.path, path.join(__dirname, 'uploads/' + filename), function(){
                
                // Check the connection with vision component.
                while(!visionConnectionAlive) {
                    console.log('Wating connection...');
                    sleep(5000);
                }

                // Get the current date.
                var dat = getDate();

                // Send image to database.
                console.log('Send image to DB.');
                var base64str = base64_encode("/usr/src/app/uploads/" + filename);
                var query = "insert into public.baxter_baxtertable(name, image, row, tag, updated, imagebin) values('" + filename + "','" + base64str + "','','','" + dat + "', '" + userID +"')";
                client.query(query, (err, res) => {
                    if(err) {
                        console.log('ERROR:');
                        console.log(err);      
                    } else {

                        // Send message to the vision component.
                        console.log(res);                        
                        console.log('Send message to the vision system.');
                        while (!skt.write(filename)) {
                            // Check the connection with vision component.
                            while(!visionConnectionAlive) {
                                console.log('Wating connection...');
                                sleep(5000);
                            }
                        }
                        
                        // Remove the image from upload folder.
                        try {
                            fs.unlinkSync("/usr/src/app/uploads/" + filename);
                            console.log('successfully deleted ' + filename);
                        } catch (err) {
                            // handle the error
                            console.log('ERROR:');
                            console.log(err);  
                        }
                    }
                });                            
                console.log('Normal finished.');
            });
        } else {
            console.log('Invalid file type.');
            fs.unlink(file.path, function(){console.log('Deleted photo')});
        }
    });

    // Handler upload erros.
    form.on('error', function(err) {
        console.log('Error occurred during processing - ' + err);
    });

    // Invoked when all the fields have been processed.
    form.on('end', function() {
        console.log('All the request fields have been processed.');
    });

    // Parse the incoming form fields.
    form.parse(req, function (err, fields, files) {
        console.log('USER_ID:', fields.user_id);
        let buff = new Buffer(fields.user_id);  
        let base64data = buff.toString('base64');
        userID = base64data;
        console.log('USER_ID_BIN:', userID);
        res.status(200).json('"response":"OK"');
    });
});

/**
 * Get authorization.
 */
app.post('/get_authorization', function (req, res) {
    // Define variables
    var query;
    var message = "false";
    var response = 200;
    
    // Get parametres.
    var userID = req.body.user_id;
    var courseID = req.body.course_id;

    // Do query
    var query = "select auth from public.baxter_studentxcoursetable where student_id = '" + userID + "' and course_id = '" + courseID  + "'";        
    client.query(query, (err, resp) => {
        if(err) {
            console.log('ERROR select public.baxter_studentxcoursetable:');
            console.log(err);
            response = 500;      
        } else {
            if (resp.rows.length <= 0) {
                message = false;
                console.log("Doesnt exist user authorization for id:", userID);            
            } else {
                message = resp.rows[0].auth;
                console.log("Get the user authorization with id:", userID, " with value: ", message);
            }
        }
        res.status(response).send(message);
    });    
});   

/**
 * Reprocess Images Action.
 */
app.post('/reprocess-images', function (req, res) {
    console.log("Receive reprocess images command action."); 
    var response = 200;
    var message = '"state": "OK", "data": "OK"';
    var query = "select name from public.baxter_baxtertable";
    client.query(query, (err, res2) => {
        if(err) {
            console.log('Error: reprocess images action query.');
            console.log(err);
            response = 500;
            message = '"state": "ERROR", "data": "ERROR"';
            res.status(response).json(message);      
        } else {
            if (res2.rows.length > 0) {
                for(var i = 0; i < res2.rows.length; i++) {
                    console.log('Processing image: ', res2.rows[i].name, '...');
                    // Send message to the vision component.                                          
                    console.log('Send message to the vision system.');
                    while (!skt.write(res2.rows[i].name)) {
                        // Check the connection with vision component.
                        while(!visionConnectionAlive) {
                            console.log('Wating connection...');
                            sleep(5000);
                        }
                    }
                    // Delay
                    sleep(15000);
                } 
            }
            res.status(response).json(message);
        }
    });    
});

//-----------------------------------------------------------
// Define util methods.
//-----------------------------------------------------------

// function to encode file data to base64 encoded string
function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap).toString('base64');
}

// Get the current date.
function getDate() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [month, day, year].join('/');
}

// Reconect with vision component.
function reconnect() {
    attemps++;
    console.log('Attempt of connection ' + attemps + '.');
    skt.destroy();
    skt = new net.Socket();    
    skt.connect(VISION_COMPONENT_PORT, VISION_COMPONENT_HOST, function() {
        console.log('Connected');
        console.log('Conection with vision [OK].');
        visionConnectionAlive = true;
    });
    skt.on('close', function() {
        // Log.
        console.log('Connection closed.');
        visionConnectionAlive = false;        
        // Delay
        sleep(1000);        
        // Reconnect.
        reconnect();                
    });
    skt.on('data', function(data) {
        console.log('Received: ' + data);
    });
}

// Log.
console.log('');
console.log('Web server start [OK].');
console.log('');
