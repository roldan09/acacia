var DEPLOY_MODE = false;

function getDataUser() {
    var courseID = document.getElementsByClassName('course-number')[0].textContent;
    var courseName = document.getElementsByClassName('course-name')[0].textContent;
    var userName = document.getElementsByClassName('username')[0].textContent;
    userID = courseID + "-" + userName;
    userID = userID.replace(/\s/g, '');
    userID = userID.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    return 'user_name=' + userName + '&user_id=' + userID + '&course_id=' + courseID + '&course_name=' + courseName;
}

function sendAuthorization(valAut) {
    var data = getDataUser() + '&val=' + valAut;
    var url = 'https://localhost:7000/set_authorization';
    if(DEPLOY_MODE) {
        url = 'https://ritaportal.udistrital.edu.co:10198/set_authorization';
    }  
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send(data);
    console.log('[INFO]:: Sent authorization data.');
}