var connection;
var USER_ID = null;
var DEPLOY_MODE = false;
var DO_SNAPSHOT_PERIOD = 8000;
var player = document.getElementById('player');
var snapshotCanvas = document.getElementById('snapshot');

var handleSuccess = function(stream) {
  try {
    if ("srcObject" in player) {
      player.srcObject = stream;
    } else {
      player.src = window.URL.createObjectURL(stream);
    }
    getUserID();
    setInterval(doSnapshot, DO_SNAPSHOT_PERIOD);
    console.log("[INFO]:: Acacia Plugin is ready");
  } catch(err) {
    console.log("[ERROR]:: " + err.name + ": " + err.message);
  }
};

var handleError = function(e) {
  console.log("[ERROR]:: " + e.name + ": " + e.message);
  alert("Acacia Plugin not supported");
};

function getUserID() {
  var courseID = document.getElementsByClassName('course-number')[0].textContent;
  var userName = document.getElementsByClassName('username')[0].textContent;
  USER_ID = courseID + "-" + userName;
  USER_ID = USER_ID.replace(/\s/g, '');
  USER_ID = USER_ID.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
}

function doSnapshot() {
  var context = snapshot.getContext('2d');
  context.drawImage(player, 0, 0, snapshotCanvas.width, snapshotCanvas.height);
  var image = new Image();
  image.id = "pic"
  image.src = snapshotCanvas.toDataURL();
  document.getElementById('aja').appendChild(image);
  sendImage(snapshotCanvas.toDataURL());
  console.log('Sent image.');
}

function uploadFiles(formData) {
  var url = 'https://localhost:7000/upload_photos';
  if(DEPLOY_MODE) {
    url = 'https://ritaportal.udistrital.edu.co:10198/upload_photos';
  }  
  var request = new XMLHttpRequest();
  request.open('POST', url, true);
  request.setRequestHeader('crossDomain', 'true');
  request.setRequestHeader('processData', 'false');
  request.setRequestHeader('contentType', 'false');
  request.send(formData);
}

function sendImage(image) {
    var files = [];
    files.push(dataURLtoFile(image, 'img.png'));
    var formData = new FormData();
    if (files.length === 0) {
        alert('Select atleast 1 file to upload.');
        return false;
    }
    if (files.length > 3) {
        alert('You can only upload up to 3 files.');
        return false;
    }
    for (var i=0; i < files.length; i++) {
        var file = files[i];
        formData.append('photos[]', file, file.name);
    }
    formData.append('user_id', USER_ID);
    uploadFiles(formData);
}

function dataURLtoFile(dataurl, filename) {
  var arr = dataurl.split(',');
  var mime = arr[0].match(/:(.*?);/)[1];
  var bstr = atob(arr[1]);
  var n = bstr.length;
  var u8arr = new Uint8Array(n);
  while(n--){
      u8arr[n] = bstr.charCodeAt(n);
  }
  return new File([u8arr], filename, {type:mime});
}

function getDataUser() {
  var rtn = '';
  var courseID = document.getElementsByClassName('course-number')[0].textContent;
  var courseName = document.getElementsByClassName('course-name')[0].textContent;
  var userName = document.getElementsByClassName('username')[0].textContent;
  rtn = courseID + "-" + userName;
  rtn = rtn.replace(/\s/g, '');
  rtn = rtn.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
  return 'user_name=' + userName + '&user_id=' + rtn + '&course_id=' + courseID + '&course_name=' + courseName;
}

function initMedia() {
  var data = getDataUser();
  var url = 'https://localhost:7000/get_authorization';
  if(DEPLOY_MODE) {
      url = 'https://ritaportal.udistrital.edu.co:10198/get_authorization';
  }  
  var request = new XMLHttpRequest();
  request.open('POST', url, true);
  request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  request.send(data);
  console.log('[INFO]:: Sent authorization data.');
  request.onload = function() {
    if (request.status != 200) { // analyze HTTP status of the response
      console.log('[ERROR]:: ${request.status}: ${request.statusText}');
      alert('Error ${request.status}: ${request.statusText}'); // e.g. 404: Not Found
    } else { // show the result
      if (request.responseText === 'true') {
        console.log('[INFO]:: Video capture starting...');
        navigator.mediaDevices.getUserMedia({video: true}).then(handleSuccess).catch(handleError);
        console.log('[INFO]:: Video capture start [OK]');
      } else {
        console.log('[INFO]:: The user has not authorized the video capture.');
      }     
    }
  };
}

initMedia();

