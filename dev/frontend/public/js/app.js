var DEPLOY_MODE = true;

function send(action, data, resposneHandler, errorHandler) {
    //var csrftoken = getCsrf(); 
    //data += '&csrfmiddlewaretoken=' + csrftoken;
    var url = 'https://localhost:7000/' + action;
    if(DEPLOY_MODE) {
        url = 'https://ritaportal.udistrital.edu.co:10198/' + action;
    }    
    var request = new XMLHttpRequest();
    request.open('POST', url, true);
    //request.setRequestHeader("X-CSRFToken", csrftoken);
    request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');    
    request.send(data);
    request.onload = function() {
        if (request.status != 200) {
            console.log('[ERROR]::', request.status,':', request.statusText);
            alert('ERROR');
        } else {
            console.log('[INFO]:: Api response: ', request.responseText);            
            var res = JSON.parse(request.response);
            if (res.state === 'OK') {
                if (resposneHandler) {
                    resposneHandler(res.data);
                }
            } else {
                if (errorHandler) {
                    errorHandler(res.data);
                }
            }      
        }
    }
    console.log('[INFO]:: Sent data ', data);    
}

function reprocessImagesResponse(data) {
    console.log('[INFO]:: Response: ', data);
}

function reprocessImagesAction() {
    send('reprocess-images', '', reprocessImagesResponse, null)
}
