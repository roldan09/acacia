---------------------------------------
-- NO CHANGE
---------------------------------------

select count(*) from baxter_baxtertable;

select count(*) from baxter_baxtertable where tag <> '';

select count(*) from baxter_baxtertable where row <> '';

select process from baxter_baxtertable;

select imagebin from baxter_baxtertable;

select * from django_migrations;

select name, row from baxter_baxtertable where name = '1555969697529-img.png';

select * from baxter_userperformancetable;

select name, tag, row from baxter_baxtertable where row = '';

select be.id 
from baxter_baxtertable as bb inner join baxter_expertbindtable as be
on bb.id = be.row_id
where bb.row = '';

select * from baxter_usertable;

select * from baxter_coursetable;

select * from baxter_userperformancetable;

select * from baxter_cursorperformancetable;

select * from baxter_studentxcoursetable;

---------------------------------------
-- CHANGE
---------------------------------------
delete from baxter_expertbindtable where id in (
    select be.id 
    from baxter_baxtertable as bb inner join baxter_expertbindtable as be
    on bb.id = be.row_id
    where bb.row = ''
)

delete from baxter_baxtertable where row = '';

update baxter_baxtertable set row = '';
