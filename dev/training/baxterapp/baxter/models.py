from django.db import models

class BaxterTable(models.Model):
	name = models.CharField(max_length=100)
	image = models.CharField(max_length=500000)
	row = models.CharField(max_length=1000)
	tag = models.CharField(max_length=20)
	updated = models.DateTimeField(auto_now=True)
	imagebin = models.BinaryField(blank=True)
	process = models.BooleanField(default=False)

class Expert(models.Model):
	gender = models.CharField(max_length=1)
	age = models.CharField(max_length=5)
	profession = models.CharField(max_length=1)
	scholarship = models.CharField(max_length=50)
	updated = models.DateTimeField(auto_now=True)

class ExpertBindTable(models.Model):
	row = models.ForeignKey(BaxterTable, on_delete=models.CASCADE)
	expert = models.ForeignKey(Expert, on_delete=models.CASCADE)
	updated = models.DateTimeField(auto_now=True)
		
class CourseTable(models.Model):
	course_id = models.CharField(max_length=50, primary_key=True)
	course_name = models.CharField(max_length=100)
	updated = models.DateTimeField(auto_now=True)

class TeacherTable(models.Model):
	teacher_id = models.CharField(max_length=50, primary_key=True)
	user_name = models.CharField(max_length=50)
	updated = models.DateTimeField(auto_now=True)

class TeacherXCourseTable(models.Model):
	course = models.ForeignKey(CourseTable, on_delete=models.CASCADE)
	teacher = models.ForeignKey(TeacherTable, on_delete=models.CASCADE)

class UserTable(models.Model):
	user_id = models.CharField(max_length=50, primary_key=True)
	user_name = models.CharField(max_length=50)	
	updated = models.DateTimeField(auto_now=True)

class StudentXCourseTable(models.Model):
	course = models.ForeignKey(CourseTable, on_delete=models.CASCADE)
	student = models.ForeignKey(UserTable, on_delete=models.CASCADE)
	auth = models.BooleanField()

class UserPerformanceTable(models.Model):
	session = models.DateTimeField(auto_now=True)
	studentxcourse = models.ForeignKey(StudentXCourseTable, on_delete=models.CASCADE)
	concentrated = models.DecimalField(max_digits=4, decimal_places=3)
	anxious = models.DecimalField(max_digits=4, decimal_places=3)
	boring = models.DecimalField(max_digits=4, decimal_places=3)

class CursorPerformanceTable(models.Model):
	session = models.DateTimeField(auto_now=True)
	course = models.CharField(max_length=50, default='testCourse')
	student = models.ForeignKey(UserTable, on_delete=models.CASCADE)
	alias = models.CharField(max_length=50, default='')
	start_date = models.CharField(max_length=50, default='')
	frequency = models.CharField(max_length=50, default='')
	state = models.CharField(max_length=50, default='')
