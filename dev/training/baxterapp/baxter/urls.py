from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('register-action', views.registerAction, name='registerAction'),
    path('save-tag', views.saveTag, name='saveTag'),
    path('get-control-board', views.getControlBoard, name='get-control-board'),
    path('load-cursor-data', views.loadCursorData, name='load-cursor-data'),
    path('load-results-data', views.loadResultsData, name='load-results-data')                
]
