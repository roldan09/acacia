function loadGauges(concentratedValue, anxiousValue, boringValue) {

    var gaugePS, concentratedBox, anxiousBox, boringBox;
    var gaugeWidth = 200;
    var gaugeHeight = 200;

    console.log('[INFO]:: concentrated: ', concentratedValue, ' anxious: ', anxiousValue, ' boring: ', boringValue);

    concentratedBox = document.getElementById("concentrated");    
    gaugePS = new RadialGauge({
        renderTo: concentratedBox,
        width: gaugeWidth,
        height: gaugeHeight,
        units: 'Concentrated',
        title: false,
        minValue: 0,
        maxValue: 100,
        exactTicks: true,
        majorTicks: [
            '0',
            '10',
            '20',
            '30',
            '40',
            '50',
            '60',
            '70',
            '80',
            '90',
            '100'
        ],
        minorTicks: 1,
        ticksAngle: 270,
        startAngle: 45,
        strokeTicks: true,
        highlights: false,
        valueInt: 3,
        valueDec: 0,
        colorPlate: "#fff",
        colorMajorTicks: "#000",
        colorMinorTicks: "#686868",
        colorTitle: "#000",
        colorUnits: "#000",
        colorNumbers: "#000",
        valueBox: true,
        colorValueText: "#000",
        colorNeedle: "rgba(240, 128, 128, 1)",
        colorNeedleEnd: "rgba(255, 160, 122, .9)",
        borderShadowWidth: 0,
        borders: true,
        borderInnerWidth: 3,
        borderMiddleWidth: 3,
        borderOuterWidth: 3,
        needleType: "arrow",
        needleWidth: 4,
        needleCircleSize: 10,
        needleCircleOuter: true,
        needleCircleInner: true,
        fontNumbersSize: 20,
        fontTitleSize: 19,
        fontUnitsSize: 19,
        fontValueSize: 28,
        animationDuration: 0,
        animationSpeed: 32
    });
    gaugePS.draw();
    gaugePS.value = '' + concentratedValue;

    
    anxiousBox = document.getElementById("anxious");        
    gaugePS = new RadialGauge({
        renderTo: anxiousBox,
        width: gaugeWidth,
        height: gaugeHeight,
        units: 'Anxious',
        title: false,
        minValue: 0,
        maxValue: 100,
        exactTicks: true,
        majorTicks: [
            '0',
            '10',
            '20',
            '30',
            '40',
            '50',
            '60',
            '70',
            '80',
            '90',
            '100'
        ],
        minorTicks: 1,
        ticksAngle: 270,
        startAngle: 45,
        strokeTicks: true,
        highlights: false,
        valueInt: 3,
        valueDec: 0,
        colorPlate: "#fff",
        colorMajorTicks: "#000",
        colorMinorTicks: "#686868",
        colorTitle: "#000",
        colorUnits: "#000",
        colorNumbers: "#000",
        valueBox: true,
        colorValueText: "#000",
        colorNeedle: "rgba(240, 128, 128, 1)",
        colorNeedleEnd: "rgba(255, 160, 122, .9)",
        borderShadowWidth: 0,
        borders: true,
        borderInnerWidth: 3,
        borderMiddleWidth: 3,
        borderOuterWidth: 3,
        needleType: "arrow",
        needleWidth: 4,
        needleCircleSize: 10,
        needleCircleOuter: true,
        needleCircleInner: true,
        fontNumbersSize: 20,
        fontTitleSize: 19,
        fontUnitsSize: 19,
        fontValueSize: 28,
        animationDuration: 0,
        animationSpeed: 32
    });
    gaugePS.draw();
    gaugePS.value = '' + anxiousValue;


    boringBox = document.getElementById("boring");        
    gaugePS = new RadialGauge({
        renderTo: boringBox,
        width: gaugeWidth,
        height: gaugeHeight,
        units: 'Boring',
        title: false,
        minValue: 0,
        maxValue: 100,
        exactTicks: true,
        majorTicks: [
            '0',
            '10',
            '20',
            '30',
            '40',
            '50',
            '60',
            '70',
            '80',
            '90',
            '100'
        ],
        minorTicks: 1,
        ticksAngle: 270,
        startAngle: 45,
        strokeTicks: true,
        highlights: false,
        valueInt: 3,
        valueDec: 0,
        colorPlate: "#fff",
        colorMajorTicks: "#000",
        colorMinorTicks: "#686868",
        colorTitle: "#000",
        colorUnits: "#000",
        colorNumbers: "#000",
        valueBox: true,
        colorValueText: "#000",
        colorNeedle: "rgba(240, 128, 128, 1)",
        colorNeedleEnd: "rgba(255, 160, 122, .9)",
        borderShadowWidth: 0,
        borders: true,
        borderInnerWidth: 3,
        borderMiddleWidth: 3,
        borderOuterWidth: 3,
        needleType: "arrow",
        needleWidth: 4,
        needleCircleSize: 10,
        needleCircleOuter: true,
        needleCircleInner: true,
        fontNumbersSize: 20,
        fontTitleSize: 19,
        fontUnitsSize: 19,
        fontValueSize: 28,
        animationDuration: 0,
        animationSpeed: 32
    });
    gaugePS.draw();
    gaugePS.value = '' + boringValue;

}
