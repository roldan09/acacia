var btnbox;
var gaugebox;
var request;
var courseID;
var DEPLOY_MODE = true;
var panelbox = document.createElement("div");
var table   = document.createElement("table");
var tblBody = document.createElement("tbody");

var messages = [];
messages['a'] = '(ü)  A través del chat póngase en comunicación con el estudiante e indague  si son razones de orden personal o de los contenidos y procesos del curso los que ocasionan los estados emocionales del estudiante. Si son razones de índole personal establezca una breve charla solidaria en la que el estudiante manifieste la situación por la que pasa y anímelo, comprensivamente, a superar la circunstancia que atraviesa. (ü)  Si los estados emocionales los genera el curso conteste las dudas que tenga el estudiante sobre  contenidos y procesos y propóngale una actividad adicional que le permita a él verificar la comprensión de los contenidos y superar los estados emocionales negativos. (ü)  Considere variar las actividades que contengan solo lectura de textos, proponga la mediación de contenidos audiovisuales.';
messages['c'] = '(ü)  Envíe al estudiante un mensaje de felicitación, una palabra positiva(¡excelente, Qué bien, Buen trabajo, Genial, Bravo!) y comente y exalte alguna de las buenas respuestas o procesos realizados por el estudiante.';
messages['ac'] = '(ü)  Envíe un mensaje de pregunta para conocer razones por las cuales se pierde con facilidad la concentración y se genera desmotivación con las actividades del curso. Las razones pueden ser académicas o personales. En ambos casos, conocer las motivaciones de los estudiantes permite hacer sugerencias más asertivas.';
messages['mac'] = '(ü)  Proponga, semanalmente, una actividad audiovisual (que contenga, en lo posible, acción o humor) de máximo 15 minutos y lance algunas preguntas, sobre el contenido del recurso, que impliquen mayor reto en la elaboración de las respuestas o procesos para el estudiante. (ü)  Utilizando el foro, establezca un proceso de seguimiento semanal de los avances realizados por el estudiante. (ü)  Proponga, en el diseño del curso, momentos de reflexión relacionados con acciones que involucran el proceso de formación a través de recursos e-learning, de sus implicaciones en relación con los recursos tecnológicos y aquellos de orden cognitivo y emocional para el aprendizaje más autónomo. (ü)  Proponga actividades con mayor desafío.';
messages['amc'] = '(ü)  Envíe al estudiante un mensaje de felicitación, una palabra positiva(¡excelente, Qué bien, Buen trabajo, Genial, Bravo!) comente y exalte alguna de las buenas respuestas o procesos realizados por el estudiante y anímelo a revisar las respuestas no acertadas o los procesos poco refinados.';
messages['nn'] = '(ü)  A través del chat póngase en comunicación con el estudiante e indague cuál es la emoción dominante que manifiesta: molestia, aversión, ira, tristeza frente a los contenidos y procesos implicados en el desarrollo del curso. (ü)  Indague si son razones de orden personal los que ocasionan los estados emocionales y no el curso. Si son razones personales, establezca una breve charla solidaria en la que el estudiante manifieste la situación por la que pasa y anímelo, comprensivamente, a superar la circunstancia que atraviesa. (ü)  Proponga al estudiante actividades (incluso adicionales) que le permitan aumentar sus conocimientos y motivaciones acerca de la perspectiva de la asignatura en la formación profesional. (ü)  Sugiera acciones orientadas a recordar los propósitos específicos de formación del curso. (ü)  Proponga actividades, fuera del formato del curso virtual, que involucren uso de recursos audiovisuales o con otras tecnologías, sugeridos por el estudiante y téngalos en cuenta como contribución, del mismo,  en su proceso formativo y evaluativo. (ü)  Elabore algunas propuestas donde se incremente la interacción a través del foro.';

function getDataUser() {
    courseID = document.getElementsByClassName('course-number')[0].textContent;
    return 'course_id=' + courseID;
}

function btnOnClic() {
    panelbox.removeChild(gaugebox);
    panelbox.removeChild(btnbox);
    table.classList.remove("disiable");
}

function starOver() {
    var img;
    var ids = this.id;
    var id = parseInt(ids[1]);
    for (let i = 1; i <= 5; i++) {
        img = document.getElementById("s"+i);
        img.classList.remove('slect-action');       
    }
    for (let i = 1; i <= id; i++) {
        img = document.getElementById("s"+i);
        img.classList.add('slect-action');       
    }
}

function loadResultsPanel() {
    if (request.status != 200) { // analyze HTTP status of the response
        console.log('[ERROR]:: ${request.status}: ${request.statusText}');
        alert('Error ${request.status}: ${request.statusText}'); // e.g. 404: Not Found
    } else { // show the result
        res = JSON.parse(request.response);
        if (res.state === 'OK') {

            var concentratedBox, anxiousBox, boringBox, canvas, msgBox, text, node, img;
            var starImg = 'static/baxter/img/star.png';

            gaugebox = document.createElement("div");
            gaugebox.setAttribute("id",'gauge-panel');
            
            concentratedBox = document.createElement("div");
            gaugebox.setAttribute("id",'concentrated-ele');
            concentratedBox.classList.add('gauge-box');
            canvas = document.createElement("canvas");
            canvas.setAttribute("id",'concentrated');
            concentratedBox.appendChild(canvas);
            gaugebox.appendChild(concentratedBox);
            
            anxiousBox = document.createElement("div");
            gaugebox.setAttribute("id",'anxious-ele');
            anxiousBox.classList.add('gauge-box');
            canvas = document.createElement("canvas");
            canvas.setAttribute("id",'anxious');
            anxiousBox.appendChild(canvas);
            gaugebox.appendChild(anxiousBox);

            boringBox = document.createElement("div");
            gaugebox.setAttribute("id",'boring-ele');
            boringBox.classList.add('gauge-box');
            canvas = document.createElement("canvas");
            canvas.setAttribute("id",'boring');
            boringBox.appendChild(canvas);
            gaugebox.appendChild(boringBox);    

            panelbox.appendChild(gaugebox);

            loadGauges(res.concentrated, res.anxious, res.boring);

            var optCus = '';
            // a, c, ac, mac, amc, nn
            if(res.concentrated < 5 && res.boring > 5) {
                optCus = 'a';
            }
            if(res.concentrated > 5 && res.boring < 5) {
                optCus = 'c';
            }
            if(res.concentrated > 5 && res.boring > 5) {
                optCus = 'ac';
                if(res.concentrated > res.boring) {
                    optCus = 'amc';
                } else {
                    optCus = 'mac';
                }
            }
            if(res.concentrated < 5 && res.boring < 5) {
                optCus = 'nn';
            }

            msgBox = document.createElement("div");
            msgBox.classList.add('msg-box');
            text = document.createElement("p");
            text.classList.add('text-box');
            node = document.createTextNode(messages[optCus]);
            text.appendChild(node);
            msgBox.appendChild(text);
            gaugebox.appendChild(msgBox);

            img = document.createElement("img");
            img.setAttribute("id",'s1');
            img.classList.add('img-box');
            img.onmouseover = starOver;
            img.src = starImg;
            msgBox.append(img);
            img = document.createElement("img");
            img.setAttribute("id",'s2');
            img.classList.add('img-box');
            img.onmouseover = starOver;
            img.src = starImg;
            msgBox.append(img);
            img = document.createElement("img");
            img.setAttribute("id",'s3');
            img.classList.add('img-box');
            img.onmouseover = starOver;
            img.src = starImg;
            msgBox.append(img);
            img = document.createElement("img");
            img.setAttribute("id",'s4');
            img.classList.add('img-box');
            img.onmouseover = starOver;
            img.src = starImg;
            msgBox.append(img);
            img = document.createElement("img");
            img.setAttribute("id",'s5');
            img.classList.add('img-box');
            img.onmouseover = starOver;
            img.src = starImg;
            msgBox.append(img);

            btnbox = document.createElement("div");
            btnbox.classList.add('btn-box');
            panelbox.appendChild(btnbox);
            var btn = document.createElement("button");
            btn.innerHTML = "Regresar";
            btn.onclick = btnOnClic;
            btnbox.appendChild(btn);
            
        } else {
            alert('No existen datos del estudiante.')
            console.log('[INFO]:: No result data.');
        }
    }
}

function rowOnClic() {
    var userID = this.id;
    table.classList.add('disiable');
    var url = 'http://localhost:7001/load-results-data';
    if(DEPLOY_MODE) {
        url = 'http://ritaportal.udistrital.edu.co:10199/load-results-data';
    }  
    request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send('user_id=' + userID + '&course_id=' + courseID);
    console.log('[INFO]:: Sent load result data.');
    request.onload = loadResultsPanel;
}

function loadCursorTable () {
    if (request.status != 200) { // analyze HTTP status of the response
        console.log('[ERROR]:: ${request.status}: ${request.statusText}');
        alert('Error ${request.status}: ${request.statusText}'); // e.g. 404: Not Found
    } else { // show the result
        res = JSON.parse(request.response);
        if (res.state === 'OK') {
            for (var i = 0; i < res.data.length; i++) {
                var row = document.createElement("tr");
                row.classList.add('row-box');
                row.setAttribute("id", res.data[i].student);
                row.onclick = rowOnClic;
        
                var celd = document.createElement("td");
                celd.classList.add('celd-box');
                var celdText = document.createTextNode(res.data[i].alias);
                celd.appendChild(celdText);
                row.appendChild(celd);
        
                celd = document.createElement("td");
                celd.classList.add('celd-box');
                celdText = document.createTextNode(res.data[i].start_date.substring(0,10));
                celd.appendChild(celdText);
                row.appendChild(celd);
        
                celd = document.createElement("td");
                celd.classList.add('celd-box');
                celdText = document.createTextNode(res.data[i].frequency);
                celd.appendChild(celdText);
                row.appendChild(celd);
        
                celd = document.createElement("td");
                celd.classList.add('celd-box');
                celdText = document.createTextNode(res.data[i].state);
                celd.appendChild(celdText);
                row.appendChild(celd);
        
                tblBody.appendChild(row);
            }
            table.appendChild(tblBody);
            table.classList.add('table-box');

        } else {
            console.log('[INFO]:: No cursor data.');
        }     
    }
}

function loadCursorData() {
    var data = getDataUser();
    var url = 'http://localhost:7001/load-cursor-data';
    if(DEPLOY_MODE) {
        url = 'http://ritaportal.udistrital.edu.co:10199/load-cursor-data';
    }  
    request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.send(data);
    console.log('[INFO]:: Sent load cursor data.');
    request.onload = loadCursorTable;
}

function init() {
    var text;
    var boardBox = document.getElementsByClassName("board-box")[0];
    
    text = document.createTextNode("Cuadro de Control");
    var h1 = document.createElement("H1");
    h1.classList.add('title-h1');
    h1.appendChild(text);
    boardBox.appendChild(h1);
    
    panelbox.classList.add('panel-box');
    boardBox.appendChild(panelbox);
    
    var titleRow = document.createElement("tr");
    titleRow.classList.add('title-row-box');
    var titleCeld = document.createElement("td");
    titleCeld.classList.add('title-celd-box');
    var titleCeldText = document.createTextNode("Alias");
    titleCeld.appendChild(titleCeldText);
    titleRow.appendChild(titleCeld);
    
    titleCeld = document.createElement("td");
    titleCeld.classList.add('title-celd-box');
    titleCeldText = document.createTextNode("Inicio");
    titleCeld.appendChild(titleCeldText);
    titleRow.appendChild(titleCeld);
    
    titleCeld = document.createElement("td");
    titleCeld.classList.add('title-celd-box');
    titleCeldText = document.createTextNode("Frecuencia");
    titleCeld.appendChild(titleCeldText);
    titleRow.appendChild(titleCeld);

    titleCeld = document.createElement("td");
    titleCeld.classList.add('title-celd-box');
    titleCeldText = document.createTextNode("Estado");
    titleCeld.appendChild(titleCeldText);
    titleRow.appendChild(titleCeld);
    
    tblBody.appendChild(titleRow);

    loadCursorData();    
    panelbox.appendChild(table);
}

init();