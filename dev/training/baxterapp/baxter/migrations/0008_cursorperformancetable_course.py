# Generated by Django 2.1.7 on 2019-05-13 16:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('baxter', '0007_auto_20190513_1525'),
    ]

    operations = [
        migrations.AddField(
            model_name='cursorperformancetable',
            name='course',
            field=models.CharField(default='testCourse', max_length=50),
        ),
    ]
