from django.contrib import admin

from baxter.models import *

import csv
from django.http import HttpResponse

# Register your models here.
admin.site.register(Expert)
admin.site.register(ExpertBindTable)

# Set properties
admin.site.site_header = 'Baxter Administrator'
admin.site.site_title = 'Baxter Administrator' 

class ExportCsvMixin:
    def export(self, request, queryset):
        meta = self.model._meta
        field_names = [field.name for field in meta.fields]
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}.txt'.format(meta)
        writer = csv.writer(response)
        txt = ''
        for obj in queryset:
            txt = txt + '' + obj.row.replace('\n', '') + '[Cluster ' + obj.tag + ']' + '\n'
        response.write(txt)
        return response
    export.short_description = "Export Selected"

@admin.register(BaxterTable)
class TableAdmin(admin.ModelAdmin, ExportCsvMixin):
    actions = ["export"]