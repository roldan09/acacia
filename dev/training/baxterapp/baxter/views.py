# 
# =========================================================
# = Universidad Distrital Francisco Jose de Caldas        =
# = Project: ACACIA                                       =
# = @ All rights reserved. April 2019.                    =
# = Autor: Ing. Fabian Jose Roldan Piñeros.               =
# = Version: 1.0                                          =
# =========================================================
#

#
# Component that represent the presentation for experts
# users for classify the images.
#

# Import modules.
from django.shortcuts import render
from django.http import HttpResponse
from baxter.models import BaxterTable
from baxter.models import CursorPerformanceTable
from baxter.models import UserPerformanceTable
from django.http import HttpResponseRedirect
from baxter.models import *
import os
import base64
from django.views.decorators.csrf import csrf_exempt
import json

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Define the simple mobil average window
SMA_WIN = 7

# Show index view 
def index(request):
	ages = []
	for x in range(18,99):
		ages.append(x)
	context = {
 		'ages' : ages
	}	
	return render(request, 'baxter/index.html', context)

# Method that load the image to classify
def loadData(request, exprt):
	# Get row
	rw = None
	entity = None
	rows = BaxterTable.objects.all()
	for x in range(0,len(rows)):
		if rows[x].tag == 'n' or rows[x].tag == '':			
			# Buid the temp folder.
			directory = os.path.join(BASE_DIR, 'baxter', 'static', 'baxter', 'res')
			# Save the image in temp folder.
			try:
				with open(os.path.join(directory, rows[x].name), "wb") as fh:
					fh.write(base64.b64decode(rows[x].image)) 
			except Exception as e:  
				print(str(e))
			# Build the response.
			entity = {
				'row' : rows[x].id,
				'expert' : exprt.id,
				'image' : rows[x].name
			}
			rw = rows[x]
			break
	
	# Check data
	if rw == None or entity == None:
		return HttpResponseRedirect('/#nomore')

	# Log bind execute
	ebt = ExpertBindTable (
		row = rw,
		expert = exprt
	)
	ebt.save()
	# Return view
	context = {
		'entity' : entity
	}
	return render(request, 'baxter/main.html', context)

# Action for register expert user data
def registerAction(request):
	# Get variables from post method
	ag = request.POST['age']
	gder = request.POST['gender']
	prssion = request.POST['profession']
	schoship = request.POST['scholarship']
	# Create the expert data
	exprt = Expert (
		age = ag,
		gender = gder,
		profession = prssion,
		scholarship = schoship
	)
	exprt.save()
	# Return view
	return loadData(request, exprt)

# Action for save tag maked by expert
def saveTag(request):
	# Get variables from post method
	tag = request.POST['tag']
	entityID = request.POST['row']
	expertID = request.POST['expert']
	# Get row and update tag
	entity = BaxterTable.objects.get(id = entityID)
	entity.tag = tag
	entity.save()
	# Get expert
	exprtQS = Expert.objects.filter(id = expertID)
	exprt = exprtQS[0]	
	return loadData(request, exprt)

# Show test board view 
def getControlBoard(request):
	course = request.GET['course']	
	context = {
 		'course' : course
	}	
	return render(request, 'baxter/board.html', context)

# Return the control board data
@csrf_exempt
def loadCursorData(request):
	entities = []
	courseId = request.POST['course_id']
	cpQS = CursorPerformanceTable.objects.filter(course = courseId)
	for x in range(0,len(cpQS)):
		entities.append({
			'student': cpQS[x].student.user_id,
			'alias' : cpQS[x].alias,
			'start_date' : cpQS[x].start_date,
			'frequency' : cpQS[x].frequency,
			'state' : cpQS[x].state
		})
	ret = {'state': 'OK', 'data': entities}			
	return HttpResponse(json.dumps(ret))
 
# Return the result data
@csrf_exempt
def loadResultsData(request):
	# Define variables
	concentrated = 0
	anxious = 0
	boring = 0
	userId = request.POST['user_id']
	courseId = request.POST['course_id']
	# Get data
	uptQS = UserPerformanceTable.objects.filter(studentxcourse__course=courseId, studentxcourse__student=userId)	
	# Calculate SMA
	for x in range(0,len(uptQS)):
		if x < (SMA_WIN-1):
			concentrated = concentrated + float(uptQS[x].concentrated)
			anxious = anxious + float(uptQS[x].anxious)
			boring = boring + float(uptQS[x].boring)		
		if x >= (SMA_WIN-1):
			concentrated = (concentrated + float(uptQS[x].concentrated))/ SMA_WIN
			anxious = (anxious + float(uptQS[x].anxious))/ SMA_WIN
			boring = (boring + float(uptQS[x].boring))/ SMA_WIN
	
	# Normalize
	concentrated = concentrated * 100
	anxious = anxious * 100
	boring = boring * 100

	# Retrun result	
	ret = {
		'state': 'OK', 
		'concentrated': concentrated,
		'anxious': anxious,
		'boring': boring
	}		
	return HttpResponse(json.dumps(ret))
