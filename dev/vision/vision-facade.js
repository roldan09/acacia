/** 
 * =========================================================
 * = Universidad Distrital Francisco Jose de Caldas        =
 * = Project: ACACIA                                       =
 * = @ All rights reserved. April 2019.                    =
 * = Autor: Ing. Fabian Jose Roldan Piñeros.               =
 * = Version: 1.0                                          =
 * =========================================================
*/

/**
 * Component that represent the artificial vision machine.
 */

//-----------------------------------------------------------
// Define of the required node js components.
//-----------------------------------------------------------
var net = require('net');
var fs = require('fs');

//-----------------------------------------------------------
// Starting log.
//-----------------------------------------------------------
console.log('');
console.log('');
console.log('***********************************************');
console.log('***********************************************');
console.log('');
console.log('Date: ' + getDate());
console.log('Vision component starting...');
console.log('');

//-----------------------------------------------------------
// Define of the database manager.
//-----------------------------------------------------------
const pgClient = require('pg').Client;
const { exec } = require('child_process');
const client = new pgClient({
  user: 'postgres',
  host: 'acacia-app_db_1',
  database: 'baxterdb',
  password: 'example',
  port: 5432,
});
client.connect();

//-----------------------------------------------------------
// Define of the end point server component.
//-----------------------------------------------------------
var server = net.createServer(function(socket) {
  
  // Sent message for indicate that connection is successful
  socket.write('Echo server\r\n');
	socket.pipe(socket);

  // Handler o incoming data.
	socket.on('data', function(data) {

      // Set the file name.
      var fileName = data;

      // Log.
    	console.log('Received: ' + fileName);
      
      // Get image from database.
	    console.log('Get image from DB.');
      var query = "select * from public.baxter_baxtertable where name = '" + fileName + "'";
      client.query(query, (err, res) => {
        if (err) {
          console.log('ERROR:');
          console.log(err);
        } else {
          console.log('---------------');
          var rows = res['rows'];
          if (rows) {
            // Log
            console.log('Create temporal image: ', rows[0].name);
            // Save the image in tem folder.
            base64_decode(rows[0].image, 'temp/' + rows[0].name);
            // Indicates to vision componente to process image.
            var command = 'cat /usr/src/app/temp/' + rows[0].name + ' | /detect.sh';            
            exec(command, (err, stdout, stderr) => {
              if (err) {
                // node couldn't execute the command
                console.log('ERROR ', err);
                return;
              }

              // the *entire* stdout and stderr (buffered)
              if (stdout) {
                // Get the response.
                var res = stdout.split("\n");
                res = res.length > 1 ? res[1] : null;

                // Ckeck the response
                if (res) {
                  // Check data quality.
                  if(checkDataQuality(res)) {
                    // Log.
                    console.log('');
                    console.log('Vector: ', rows[0].name, 'stout: ', res);
                    console.log('');
                  } else {
                    stderr = 'Poor data quality for image: ' + rows[0].name;  
                  }
                } else {
                  stderr = 'Not valid data for image: ' + rows[0].name;
                }
              }

              // Check error.
              if (stderr) {
                // Log error.
                console.log('WARN image: ', rows[0].name, ' stderr: ', stderr);

                // Delete register from db
                // Update DB.
                query = "DELETE FROM public.baxter_baxtertable WHERE name = '" + rows[0].name + "'";
                client.query(query, (err, res) => {
                  if (err) {
                    console.log('ERROR: DELETE FROM public.baxter_baxtertable WHERE name');
                    console.log(err);
                  } else {
                      console.log(res); 
                      console.log('---------------');
                      console.log('Delete ', rows[0].name, ' reg database [OK]');
                  }
                });

              } else {
                // Log.
                console.log('Updating database...');
                console.log('Set row to the reg image: ', rows[0].name);

                // Update DB.
                query = "UPDATE public.baxter_baxtertable SET row = '" + stdout +"' WHERE name = '" + rows[0].name + "'";
                client.query(query, (err, res) => {
                  if (err) {
                    console.log('ERROR:');
                    console.log(err);
                  } else {
                      console.log(res); 
                      console.log('---------------');
                      console.log('Update reg: ', rows[0].name,' of database [OK]');
                  }
                });                
              }

              // Delete the image from tem folder.
              // Log.
              console.log('Deleting temp file...');
              try {
                fs.unlinkSync('/usr/src/app/temp/' + rows[0].name);
                console.log('/usr/src/app/temp/' + rows[0].name);
                // Log.
                console.log('Delete temp image: ' ,rows[0].name,' [OK]');                
              } catch (err) {
                // handle the error
                console.log('ERROR:');
                console.log(err);  
              }

            });
          }
        }
      });
	});

  // Handler the close socket.
	socket.on('close', function() {
	    console.log('Connection closed');
	});
});

// Log.
console.log('Listing on the port 7000...');
server.listen(7002);
console.log('Lsiting [OK]');

// function to create file from base64 encoded string
function base64_decode(base64str, file) {
    // Create buffer object from base64 encoded string, it is 
    // important to tell the constructor that the string is 
    // base64 encoded.
    var bitmap = new Buffer(base64str, 'base64');
    // Write buffer to file.
    fs.writeFileSync(file, bitmap);
    console.log('******** File created from base64 encoded string ********');
}

// Handler the errors of the system.
process.on('uncaughtException', function (err) {
  console.log('Caught exception: ', err);
});

// Get the current date.
function getDate() {
  var d = new Date(),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;
  return [day, month, year].join('/');
}

// Check data quality.
function checkDataQuality(info) {
  var nodata = 0;
  var dimensions = info.split(",");

  // Count the among of no data.
  for (let index = 0; index < dimensions.length; index++) {
    const element = dimensions[index];
    if (element === 'nan' || element === 'unknown') {
      nodata++;
    }    
  }

  // Calculate the data quality.
  var qa = (1 - (nodata / dimensions.length)) * 100;
  
  // Log.
  console.log('');
  console.log('Data quality: ' + qa + '%');
  console.log('');

  return qa >= 80; 
}

// Log.
console.log('');
console.log('Vision componet start [OK]');
console.log('');
