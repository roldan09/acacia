// Generated with g9.

package co.edu.distrital.dal.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity(name="baxter_userperformancetable")
public class BaxterUserperformancetable implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(nullable=false)
    private Timestamp session;
    @Column(nullable=false, precision=4, scale=3)
    private BigDecimal concentrated;
    @Column(nullable=false, precision=4, scale=3)
    private BigDecimal anxious;
    @Column(nullable=false, precision=4, scale=3)
    private BigDecimal boring;
    @ManyToOne(optional=false)
    @JoinColumn(name="studentxcourse_id", nullable=false)
    private BaxterStudentxcoursetable baxterStudentxcoursetable;

    /** Default constructor. */
    public BaxterUserperformancetable() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for session.
     *
     * @return the current value of session
     */
    public Timestamp getSession() {
        return session;
    }

    /**
     * Setter method for session.
     *
     * @param aSession the new value for session
     */
    public void setSession(Timestamp aSession) {
        session = aSession;
    }

    /**
     * Access method for concentrated.
     *
     * @return the current value of concentrated
     */
    public BigDecimal getConcentrated() {
        return concentrated;
    }

    /**
     * Setter method for concentrated.
     *
     * @param aConcentrated the new value for concentrated
     */
    public void setConcentrated(BigDecimal aConcentrated) {
        concentrated = aConcentrated;
    }

    /**
     * Access method for anxious.
     *
     * @return the current value of anxious
     */
    public BigDecimal getAnxious() {
        return anxious;
    }

    /**
     * Setter method for anxious.
     *
     * @param aAnxious the new value for anxious
     */
    public void setAnxious(BigDecimal aAnxious) {
        anxious = aAnxious;
    }

    /**
     * Access method for boring.
     *
     * @return the current value of boring
     */
    public BigDecimal getBoring() {
        return boring;
    }

    /**
     * Setter method for boring.
     *
     * @param aBoring the new value for boring
     */
    public void setBoring(BigDecimal aBoring) {
        boring = aBoring;
    }

    /**
     * Access method for baxterStudentxcoursetable.
     *
     * @return the current value of baxterStudentxcoursetable
     */
    public BaxterStudentxcoursetable getBaxterStudentxcoursetable() {
        return baxterStudentxcoursetable;
    }

    /**
     * Setter method for baxterStudentxcoursetable.
     *
     * @param aBaxterStudentxcoursetable the new value for baxterStudentxcoursetable
     */
    public void setBaxterStudentxcoursetable(BaxterStudentxcoursetable aBaxterStudentxcoursetable) {
        baxterStudentxcoursetable = aBaxterStudentxcoursetable;
    }

    /**
     * Compares the key for this instance with another BaxterUserperformancetable.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BaxterUserperformancetable and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BaxterUserperformancetable)) {
            return false;
        }
        BaxterUserperformancetable that = (BaxterUserperformancetable) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BaxterUserperformancetable.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BaxterUserperformancetable)) return false;
        return this.equalKeys(other) && ((BaxterUserperformancetable)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BaxterUserperformancetable |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
