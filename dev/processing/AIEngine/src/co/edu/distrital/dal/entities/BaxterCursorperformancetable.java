// Generated with g9.

package co.edu.distrital.dal.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity(name="baxter_cursorperformancetable")
public class BaxterCursorperformancetable implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(nullable=false)
    private Timestamp session;
    @Column(nullable=false, length=50)
    private String alias;
    @Column(nullable=false, length=50)
    private String frequency;
    @Column(name="start_date", nullable=false, length=50)
    private String startDate;
    @Column(nullable=false, length=50)
    private String state;
    @Column(nullable=false, length=50)
    private String course;
    @ManyToOne(optional=false)
    @JoinColumn(name="student_id", nullable=false)
    private BaxterUsertable baxterUsertable;

    /** Default constructor. */
    public BaxterCursorperformancetable() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for session.
     *
     * @return the current value of session
     */
    public Timestamp getSession() {
        return session;
    }

    /**
     * Setter method for session.
     *
     * @param aSession the new value for session
     */
    public void setSession(Timestamp aSession) {
        session = aSession;
    }

    /**
     * Access method for alias.
     *
     * @return the current value of alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Setter method for alias.
     *
     * @param aAlias the new value for alias
     */
    public void setAlias(String aAlias) {
        alias = aAlias;
    }

    /**
     * Access method for frequency.
     *
     * @return the current value of frequency
     */
    public String getFrequency() {
        return frequency;
    }

    /**
     * Setter method for frequency.
     *
     * @param aFrequency the new value for frequency
     */
    public void setFrequency(String aFrequency) {
        frequency = aFrequency;
    }

    /**
     * Access method for startDate.
     *
     * @return the current value of startDate
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Setter method for startDate.
     *
     * @param aStartDate the new value for startDate
     */
    public void setStartDate(String aStartDate) {
        startDate = aStartDate;
    }

    /**
     * Access method for state.
     *
     * @return the current value of state
     */
    public String getState() {
        return state;
    }

    /**
     * Setter method for state.
     *
     * @param aState the new value for state
     */
    public void setState(String aState) {
        state = aState;
    }

    /**
     * Access method for course.
     *
     * @return the current value of course
     */
    public String getCourse() {
        return course;
    }

    /**
     * Setter method for course.
     *
     * @param aCourse the new value for course
     */
    public void setCourse(String aCourse) {
        course = aCourse;
    }

    /**
     * Access method for baxterUsertable.
     *
     * @return the current value of baxterUsertable
     */
    public BaxterUsertable getBaxterUsertable() {
        return baxterUsertable;
    }

    /**
     * Setter method for baxterUsertable.
     *
     * @param aBaxterUsertable the new value for baxterUsertable
     */
    public void setBaxterUsertable(BaxterUsertable aBaxterUsertable) {
        baxterUsertable = aBaxterUsertable;
    }

    /**
     * Compares the key for this instance with another BaxterCursorperformancetable.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BaxterCursorperformancetable and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BaxterCursorperformancetable)) {
            return false;
        }
        BaxterCursorperformancetable that = (BaxterCursorperformancetable) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BaxterCursorperformancetable.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BaxterCursorperformancetable)) return false;
        return this.equalKeys(other) && ((BaxterCursorperformancetable)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BaxterCursorperformancetable |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
