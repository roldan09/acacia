/*
 * Universidad Distrital Francias Jose de caldas
 * @Todos los derechos.
 */
package co.edu.distrital.dal.manage;

import co.edu.distrital.dal.entities.BaxterBaxtertable;
import co.edu.distrital.dal.model.Tuple;
import co.edu.distrital.exception.AIException;
import co.edu.distrital.util.Functions;
import co.edu.distrital.util.Text;
import co.edu.distrital.util.Trace;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import javax.persistence.EntityManager;
import java.util.List;
import javax.persistence.Query;
import weka.core.Instances;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.converters.ConverterUtils.DataSink;

/**
 * Classs that make pre-process data.
 * 
 * @author fjroldan
 */
public class TableETL {
    
    // Vector len parametrer
    private final int VECTOR_LEN = 53;    
    // Data List
    private ArrayList<String> dataList;
    // Field List
    private ArrayList<String[]> fieldList;
    // Tags List
    private ArrayList<String> tagList;
    // Tuple list
    private ArrayList<Tuple> tuples;
    // Entity Manager
    private EntityManager entityManager;
    // Final tarining data for training
    private Instances trainingData;
    // Final test data for internal validation
    private Instances testData;
    // Final meaning dada for set meaning
    private Instances meaningData;
    // Final validation data for external validation
    private Instances validationData;
    // Set Ranges
    private int[] setRange;

    /**
     * Default constructor
     * 
     * @param entityManager Entity manager 
     */
    public TableETL(EntityManager entityManager) {
        this.entityManager = entityManager;
        dataList = new ArrayList<>();
        fieldList = new ArrayList<>();
        tagList = new ArrayList<>();
        tuples = new ArrayList<>();
        
        // Init dimensions
        ArrayList atts = new ArrayList();
        for (int j = 0; j < VECTOR_LEN; j++) {
            atts.add(new Attribute("D" + j));
        }
        
        // Init data set
        trainingData = new Instances("Training", atts, 0);
        testData = new Instances("Test", atts, 0);
        meaningData = new Instances("Meaning", atts, 0);
        validationData = new Instances("Validation", atts, 0);
    }
    
    /**
     * Load data from database.
     * @throws AIException 
     */    
    public void loadData() throws AIException {
        // Define variables
        String row;
        byte[] idByte;
        String id = "";
        byte[] decodedBytes;
        
        // Begin transaction
        entityManager.getTransaction().begin();

        // Do query
        String sql = "SELECT b from baxter_baxtertable b where process <> true";
        Query query = entityManager.createQuery(sql);        
        List<BaxterBaxtertable> infoLits =  query.getResultList();
        
        // Iterate into results
        for (int i = 0; i < infoLits.size(); i++) {
            BaxterBaxtertable info = infoLits.get(i);
            idByte = info.getImagebin();
            for (int j = 0; j < idByte.length; j++) {
                id += (char)idByte[j];
            }
            decodedBytes = Base64.getDecoder().decode(id);
            id = new String(decodedBytes) + "-" + info.getId();
            row = id + "," + info.getRow();
            System.out.println("Row: " + row);
            System.out.println();
            dataList.add(row);
            tagList.add(info.getTag());
            id = "";
        }

        // End transaction
        entityManager.getTransaction().commit();
        
        // Set ranges: [training, test, meaning, validation]
        int size = dataList.size();
        setRange = new int[4];
        setRange[0] = (int) Math.round(size * 0.8);
        setRange[1] = setRange[0] + (int) Math.round(size * 0.1);
        setRange[2] = (int) Math.round(size * 0.5);
        setRange[3] = size;
    }
    
    /**
     * Get fields from data list.
     * 
     * @param row 
     * @return 
     */
    private String[] getFields(String row) {
        String[] rtn = new String[55];
        String[] splits;
        if (row != null && row.length() > 0) {
            splits = row.split(",");
            rtn[0] = splits[0];
            for (int i = 54; i < splits.length; i++) {
                rtn[i-53] = splits[i];
            }
        }
        return rtn;
    }
    
    /**
     * Extract data from the fields.
     */
    public void extractData() throws AIException {
        String[] fields;
        for (int i = 0; i < dataList.size(); i++) {
            // Get fields
            fields = getFields(dataList.get(i));
            fieldList.add(fields);
            Text.print("FIELDS", fields);
        }
    }
    
    /**
     * Make the first process to the data.
     * @param trainigMode indicates if is training or not is 
     */
    public String process(boolean trainigMode) throws AIException {
        double[] res;
        String[] fields;
        String field;
        String baseName = null;
        String id;
        int index;
        int idxi = 0;
        for (int i = 0; i < fieldList.size(); i++) {
            // Init res vector
            res = new double[VECTOR_LEN];
            // Get fields
            fields = fieldList.get(i);
            // Process ID
            id = fields[0];
            // Process glasses
            field = fields[4];
            if (field != null) {
                field = field.trim();            
                res[0] = field.equals("no") ? 0.0 : 1;
            } else {
                res[0] = 0.5;
            }
            // Process age
            field = fields[5];
            index = -1;
            if (field != null) {
                field = field.trim();            
                switch(field) {
                    case("Unknown"):
                        index = 1;
                        break;
                    case("under 18"):
                        index = 2;
                        break;
                    case("18-24"):
                        index = 3;
                        break;
                    case("25-34"):
                        index = 4;
                        break;
                    case("35-44"):
                        index = 5;
                        break;
                    case("45-54"):
                        index = 6;
                        break;
                    case("55-64"):
                        index = 7;
                        break;
                    case("65 plus"):
                        index = 8;
                        break;
                }                
            }
            for (int j = 1; j <= 8; j++) {                
                res[j] = (j == index ? 1.0 : 0.0);                
            }
            // Process ethnicity
            field = fields[6];
            index = -1;
            if (field != null) {                            
                switch(field) {
                    case("unknown"):
                        index = 9;
                        break;
                    case("caucasian"):
                        index = 10;
                        break;
                    case("black african"):
                        index = 11;
                        break;
                    case("south asian"):
                        index = 12;
                        break;
                    case("east asian"):
                        index = 13;
                        break;
                    case("hispanic"):
                        index = 14;
                        break;                    
                }                
            }
            for (int j = 9; j <= 14; j++) {                
                res[j] = (j == index ? 1.0 : 0.0);                
            }
            // Process gender
            field = fields[7];
            index = -1;
            if (field != null) {
                field = field.trim();
                switch(field) {
                    case("unknown"):
                        index = 15;
                        break;
                    case("male"):
                        index = 16;
                        break;
                    case("female"):
                        index = 17;
                        break;                                        
                }                
            }
            for (int j = 15; j <= 17; j++) {                
                res[j] = (j == index ? 1.0 : 0.0);                
            }
            // Process pitch
            field = fields[9];
            res[18] = Functions.normalizeAngle(field);
            // Process yaw
            field = fields[10];
            res[19] = Functions.normalizeAngle(field);
            // Process roll
            field = fields[11];
            res[20] = Functions.normalizeAngle(field);
            // Process Emotions: joy, fear, disgust, sadness, anger, surprise, 
            //                   contempt, valence, engagement
            for (int j = 12; j <= 20; j++) {
                field = fields[j];
                res[j+9] = Functions.normalizeEmotion(field);
            }
            // Process Expressions: smile, innerBrowRaise, browRaise, 
            // browFurrow, noseWrinkle, upperLipRaise, lipCornerDepressor, 
            // chinRaise, lipPucker, lipPress, lipSuck, mouthOpen, smirk,
            // eyeClosure, attention, eyeWiden, cheekRaise, lidTighten,
            // dimpler, lipStretch, jawDrop. 
            for (int j = 21; j <= 41; j++) {
                field = fields[j];
                res[j+9] = Functions.normalizeExpression(field);
            }
            Text.print("VECTORS", res);
            // Create weka instance
            Instance inst = new DenseInstance(VECTOR_LEN);
            for (int w = 0; w < VECTOR_LEN; w++) {
                inst.setValue(w, res[w]);
            }
                        
            // Add to sets
            tuples.add(new Tuple(id, inst));
            if (idxi < setRange[0]) {
                trainingData.add(inst);
            } else {
                if (idxi < setRange[1]) {
                    testData.add(inst);
                } else {
                    validationData.add(inst);
                }
            }
            if (idxi < setRange[2]) {
                meaningData.add(inst);
            }
            
            // Increments instance index
            idxi++;
        }
        
        // Chec if is training mode
        if (trainigMode) {
            // Save data
            Trace trace = Trace.getInstance();
            trace.info("Saving data to file system...");
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
            Date date = new Date();
            baseName = dateFormat.format(date);
            save(baseName, "training", trainingData);
            save(baseName, "test", testData);
            save(baseName, "meaning", meaningData);
            save(baseName, "validation", validationData);
            saveTagData(baseName, tagList);
        }
        return baseName;
    }
    
    /**
     * Save data into file system.
     * @param baseName base name of the data
     * @param category set type
     * @param data data to save
     * @throws AIException if present am error
     */
    private void save(String baseName, String category, Instances data)  throws AIException {
        Trace trace = Trace.getInstance();
        trace.info("Saving data to file system...");
        String outputFilename = "assets/data/"+ baseName + "-" + category + ".arff";
        try {
            DataSink.write(outputFilename, data);
            trace.info("Data saved in: " + outputFilename);
        }
        catch (Exception e) {
            trace.erro("Failed to save data to: " + outputFilename, e);
            throw new AIException("Failed to save data to: " + outputFilename);
        }
    }
    
    
    /**
     * Save tag data into file system.
     * @param baseName base name of the data
     * @param category set type
     * @param data data to save
     * @throws AIException if present am error
     */
    private void saveTagData(String baseName, ArrayList<String> data)  throws AIException {
        Trace trace = Trace.getInstance();
        trace.info("Saving tag data to file system...");
        String outputFilename = "assets/data/"+ baseName + "-tags.obj";
        try {
            FileOutputStream fileOut = new FileOutputStream(outputFilename);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(data);
            objectOut.close();
            trace.info("Data saved in: " + outputFilename);
        }
        catch (Exception e) {
            trace.erro("Failed to save data to: " + outputFilename, e);
            throw new AIException("Failed to save data to: " + outputFilename);
        }
    }
    
    /**
     * Get data to classify process.
     * @return Dataset
     */
    public ArrayList<Tuple> getData() {
        return tuples;
    }

}
