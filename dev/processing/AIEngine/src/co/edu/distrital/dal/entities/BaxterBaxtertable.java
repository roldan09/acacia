// Generated with g9.

package co.edu.distrital.dal.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="baxter_baxtertable")
public class BaxterBaxtertable implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(nullable=false, length=100)
    private String name;
    @Column(nullable=false, length=500000)
    private String image;
    @Column(nullable=false, length=600)
    private String row;
    @Column(nullable=false, length=20)
    private String tag;
    @Column(nullable=false)
    private Timestamp updated;
    @Column(nullable=false)
    private byte[] imagebin;
    @Column(nullable=false, length=1)
    private boolean process;
    @OneToMany(mappedBy="baxterBaxtertable")
    private Set<BaxterExpertbindtable> baxterExpertbindtable;

    /** Default constructor. */
    public BaxterBaxtertable() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for image.
     *
     * @return the current value of image
     */
    public String getImage() {
        return image;
    }

    /**
     * Setter method for image.
     *
     * @param aImage the new value for image
     */
    public void setImage(String aImage) {
        image = aImage;
    }

    /**
     * Access method for row.
     *
     * @return the current value of row
     */
    public String getRow() {
        return row;
    }

    /**
     * Setter method for row.
     *
     * @param aRow the new value for row
     */
    public void setRow(String aRow) {
        row = aRow;
    }

    /**
     * Access method for tag.
     *
     * @return the current value of tag
     */
    public String getTag() {
        return tag;
    }

    /**
     * Setter method for tag.
     *
     * @param aTag the new value for tag
     */
    public void setTag(String aTag) {
        tag = aTag;
    }

    /**
     * Access method for updated.
     *
     * @return the current value of updated
     */
    public Timestamp getUpdated() {
        return updated;
    }

    /**
     * Setter method for updated.
     *
     * @param aUpdated the new value for updated
     */
    public void setUpdated(Timestamp aUpdated) {
        updated = aUpdated;
    }

    /**
     * Access method for imagebin.
     *
     * @return the current value of imagebin
     */
    public byte[] getImagebin() {
        return imagebin;
    }

    /**
     * Setter method for imagebin.
     *
     * @param aImagebin the new value for imagebin
     */
    public void setImagebin(byte[] aImagebin) {
        imagebin = aImagebin;
    }

    /**
     * Access method for process.
     *
     * @return true if and only if process is currently true
     */
    public boolean getProcess() {
        return process;
    }

    /**
     * Setter method for process.
     *
     * @param aProcess the new value for process
     */
    public void setAuth(boolean aProcess) {
        process = aProcess;
    }
    
    /**
     * Access method for baxterExpertbindtable.
     *
     * @return the current value of baxterExpertbindtable
     */
    public Set<BaxterExpertbindtable> getBaxterExpertbindtable() {
        return baxterExpertbindtable;
    }

    /**
     * Setter method for baxterExpertbindtable.
     *
     * @param aBaxterExpertbindtable the new value for baxterExpertbindtable
     */
    public void setBaxterExpertbindtable(Set<BaxterExpertbindtable> aBaxterExpertbindtable) {
        baxterExpertbindtable = aBaxterExpertbindtable;
    }

    /**
     * Compares the key for this instance with another BaxterBaxtertable.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BaxterBaxtertable and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BaxterBaxtertable)) {
            return false;
        }
        BaxterBaxtertable that = (BaxterBaxtertable) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BaxterBaxtertable.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BaxterBaxtertable)) return false;
        return this.equalKeys(other) && ((BaxterBaxtertable)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BaxterBaxtertable |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
