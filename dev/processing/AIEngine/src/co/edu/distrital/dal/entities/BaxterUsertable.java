// Generated with g9.

package co.edu.distrital.dal.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="baxter_usertable")
public class BaxterUsertable implements Serializable {

    /** Primary key. */
    protected static final String PK = "userId";

    @Id
    @Column(name="user_id", unique=true, nullable=false, length=50)
    private String userId;
    @Column(name="user_name", nullable=false, length=50)
    private String userName;
    @Column(nullable=false)
    private Timestamp updated;
    @OneToMany(mappedBy="baxterUsertable")
    private Set<BaxterCursorperformancetable> baxterCursorperformancetable;
    @OneToMany(mappedBy="baxterUsertable")
    private Set<BaxterStudentxcoursetable> baxterStudentxcoursetable;

    /** Default constructor. */
    public BaxterUsertable() {
        super();
    }

    /**
     * Access method for userId.
     *
     * @return the current value of userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Setter method for userId.
     *
     * @param aUserId the new value for userId
     */
    public void setUserId(String aUserId) {
        userId = aUserId;
    }

    /**
     * Access method for userName.
     *
     * @return the current value of userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Setter method for userName.
     *
     * @param aUserName the new value for userName
     */
    public void setUserName(String aUserName) {
        userName = aUserName;
    }

    /**
     * Access method for updated.
     *
     * @return the current value of updated
     */
    public Timestamp getUpdated() {
        return updated;
    }

    /**
     * Setter method for updated.
     *
     * @param aUpdated the new value for updated
     */
    public void setUpdated(Timestamp aUpdated) {
        updated = aUpdated;
    }

    /**
     * Access method for baxterCursorperformancetable.
     *
     * @return the current value of baxterCursorperformancetable
     */
    public Set<BaxterCursorperformancetable> getBaxterCursorperformancetable() {
        return baxterCursorperformancetable;
    }

    /**
     * Setter method for baxterCursorperformancetable.
     *
     * @param aBaxterCursorperformancetable the new value for baxterCursorperformancetable
     */
    public void setBaxterCursorperformancetable(Set<BaxterCursorperformancetable> aBaxterCursorperformancetable) {
        baxterCursorperformancetable = aBaxterCursorperformancetable;
    }

    /**
     * Access method for baxterStudentxcoursetable.
     *
     * @return the current value of baxterStudentxcoursetable
     */
    public Set<BaxterStudentxcoursetable> getBaxterStudentxcoursetable() {
        return baxterStudentxcoursetable;
    }

    /**
     * Setter method for baxterStudentxcoursetable.
     *
     * @param aBaxterStudentxcoursetable the new value for baxterStudentxcoursetable
     */
    public void setBaxterStudentxcoursetable(Set<BaxterStudentxcoursetable> aBaxterStudentxcoursetable) {
        baxterStudentxcoursetable = aBaxterStudentxcoursetable;
    }

    /**
     * Compares the key for this instance with another BaxterUsertable.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BaxterUsertable and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BaxterUsertable)) {
            return false;
        }
        BaxterUsertable that = (BaxterUsertable) other;
        Object myUserId = this.getUserId();
        Object yourUserId = that.getUserId();
        if (myUserId==null ? yourUserId!=null : !myUserId.equals(yourUserId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BaxterUsertable.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BaxterUsertable)) return false;
        return this.equalKeys(other) && ((BaxterUsertable)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getUserId() == null) {
            i = 0;
        } else {
            i = getUserId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BaxterUsertable |");
        sb.append(" userId=").append(getUserId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("userId", getUserId());
        return ret;
    }

}
