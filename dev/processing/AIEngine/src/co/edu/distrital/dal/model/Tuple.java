/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.distrital.dal.model;

import weka.core.Instance;

/**
 *
 * @author fjroldan
 */
public class Tuple {

    private String id;
    private Instance intance;

    public Tuple(String id, Instance intance) {
        this.id = id;
        this.intance = intance;
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instance getIntance() {
        return intance;
    }

    public void setIntance(Instance intance) {
        this.intance = intance;
    }

}
