// Generated with g9.

package co.edu.distrital.dal.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="baxter_teachertable")
public class BaxterTeachertable implements Serializable {

    /** Primary key. */
    protected static final String PK = "teacherId";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(name="teacher_id", unique=true, nullable=false, length=50)
    private String teacherId;
    @Column(name="user_name", nullable=false, length=50)
    private String userName;
    @Column(nullable=false)
    private Timestamp updated;
    @OneToMany(mappedBy="baxterTeachertable")
    private Set<BaxterTeacherxcoursetable> baxterTeacherxcoursetable;

    /** Default constructor. */
    public BaxterTeachertable() {
        super();
    }

    /**
     * Access method for teacherId.
     *
     * @return the current value of teacherId
     */
    public String getTeacherId() {
        return teacherId;
    }

    /**
     * Setter method for teacherId.
     *
     * @param aTeacherId the new value for teacherId
     */
    public void setTeacherId(String aTeacherId) {
        teacherId = aTeacherId;
    }

    /**
     * Access method for userName.
     *
     * @return the current value of userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Setter method for userName.
     *
     * @param aUserName the new value for userName
     */
    public void setUserName(String aUserName) {
        userName = aUserName;
    }

    /**
     * Access method for updated.
     *
     * @return the current value of updated
     */
    public Timestamp getUpdated() {
        return updated;
    }

    /**
     * Setter method for updated.
     *
     * @param aUpdated the new value for updated
     */
    public void setUpdated(Timestamp aUpdated) {
        updated = aUpdated;
    }

    /**
     * Access method for baxterTeacherxcoursetable.
     *
     * @return the current value of baxterTeacherxcoursetable
     */
    public Set<BaxterTeacherxcoursetable> getBaxterTeacherxcoursetable() {
        return baxterTeacherxcoursetable;
    }

    /**
     * Setter method for baxterTeacherxcoursetable.
     *
     * @param aBaxterTeacherxcoursetable the new value for baxterTeacherxcoursetable
     */
    public void setBaxterTeacherxcoursetable(Set<BaxterTeacherxcoursetable> aBaxterTeacherxcoursetable) {
        baxterTeacherxcoursetable = aBaxterTeacherxcoursetable;
    }

    /**
     * Compares the key for this instance with another BaxterTeachertable.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BaxterTeachertable and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BaxterTeachertable)) {
            return false;
        }
        BaxterTeachertable that = (BaxterTeachertable) other;
        Object myTeacherId = this.getTeacherId();
        Object yourTeacherId = that.getTeacherId();
        if (myTeacherId==null ? yourTeacherId!=null : !myTeacherId.equals(yourTeacherId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BaxterTeachertable.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BaxterTeachertable)) return false;
        return this.equalKeys(other) && ((BaxterTeachertable)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getTeacherId() == null) {
            i = 0;
        } else {
            i = getTeacherId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BaxterTeachertable |");
        sb.append(" teacherId=").append(getTeacherId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("teacherId", getTeacherId());
        return ret;
    }

}
