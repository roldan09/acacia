/*
 * Universidad Distrital Francias Jose de caldas
 * @Todos los derechos.
 */
package co.edu.distrital.dal.model;

import co.edu.distrital.util.Trace;

/**
 * Class that represents the histogram data for the meaning.
 *
 * @author fjroldan
 */
public class HistogramData {

    /**
     * Histogram data.
     */
    private ClusterFrecuency[] histo;

    /**
     * Default constructor.
     */
    public HistogramData() {
        histo = new ClusterFrecuency[4];
        histo[0] = new ClusterFrecuency();
        histo[1] = new ClusterFrecuency();
        histo[2] = new ClusterFrecuency();
        histo[3] = new ClusterFrecuency();
    }

    /**
     * Update the histogram.
     *
     * @param cluster Cluster to update
     * @param classTag Tag
     */
    public void update(int cluster, String classTag) {
        switch (classTag) {
            case "Concentrated":
                histo[cluster].concentratedUP();
                break;
            case "Anxious":
                histo[cluster].anxiousUP();
                break;
            case "Boring":
                histo[cluster].boringUP();
                break;
            case "Undetermined":
                histo[cluster].getUndetermined();
                break;
        }
    }

    /**
     * Print results
     *
     */
    public void printResults() {
        double size;
        Trace trace = Trace.getInstance();
        trace.info("");
        trace.info("");
        trace.info("------------------------------------------------------------");
        trace.info(" RESULTS:");
        trace.info("------------------------------------------------------------");
        trace.info("Cluster 1:");
        size = histo[0].getConcentrated()
                + histo[0].getAnxious()
                + histo[0].getBoring()
                + histo[0].getUndetermined();
        trace.info("Frecuency: " + size);
        trace.info("Concentrated: " + ((histo[0].getConcentrated() / size)* 100) + "%");
        trace.info("Anxious: " + ((histo[0].getAnxious() / size)* 100) + "%");
        trace.info("Boring: " + ((histo[0].getBoring() / size)* 100) + "%");
        trace.info("Undetermined: " + ((histo[0].getUndetermined() / size)* 100) + "%");
        trace.info("Cluster 2:");
        size = histo[1].getConcentrated()
                + histo[1].getAnxious()
                + histo[1].getBoring()
                + histo[1].getUndetermined();
        trace.info("Frecuency: " + size);
        trace.info("Concentrated: " + ((histo[1].getConcentrated() / size)* 100) + "%");
        trace.info("Anxious: " + ((histo[1].getAnxious() / size)* 100) + "%");
        trace.info("Boring: " + ((histo[1].getBoring() / size)* 100) + "%");
        trace.info("Undetermined: " + ((histo[1].getUndetermined() / size)* 100) + "%");
        trace.info("Cluster 3:");
        size = histo[2].getConcentrated()
                + histo[2].getAnxious()
                + histo[2].getBoring()
                + histo[2].getUndetermined();
        trace.info("Frecuency: " + size);
        trace.info("Concentrated: " + ((histo[2].getConcentrated() / size)* 100) + "%");
        trace.info("Anxious: " + ((histo[2].getAnxious() / size)* 100) + "%");
        trace.info("Boring: " + ((histo[2].getBoring() / size)* 100) + "%");
        trace.info("Undetermined: " + ((histo[2].getUndetermined() / size)* 100) + "%");
        trace.info("Cluster 4:");
        size = histo[3].getConcentrated()
                + histo[3].getAnxious()
                + histo[3].getBoring()
                + histo[3].getUndetermined();
        trace.info("Frecuency: " + size);
        trace.info("Concentrated: " + ((histo[3].getConcentrated() / size)* 100) + "%");
        trace.info("Anxious: " + ((histo[3].getAnxious() / size)* 100) + "%");
        trace.info("Boring: " + ((histo[3].getBoring() / size)* 100) + "%");
        trace.info("Undetermined: " + ((histo[3].getUndetermined() / size)* 100) + "%");
        trace.info("------------------------------------------------------------");
        trace.info("");
    }
}
