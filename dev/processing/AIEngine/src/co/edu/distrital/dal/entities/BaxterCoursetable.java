// Generated with g9.

package co.edu.distrital.dal.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="baxter_coursetable")
public class BaxterCoursetable implements Serializable {

    /** Primary key. */
    protected static final String PK = "courseId";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @Column(name="course_id", unique=true, nullable=false, length=50)
    private String courseId;
    @Column(name="course_name", nullable=false, length=100)
    private String courseName;
    @Column(nullable=false)
    private Timestamp updated;
    @OneToMany(mappedBy="baxterCoursetable")
    private Set<BaxterStudentxcoursetable> baxterStudentxcoursetable;
    @OneToMany(mappedBy="baxterCoursetable")
    private Set<BaxterTeacherxcoursetable> baxterTeacherxcoursetable;

    /** Default constructor. */
    public BaxterCoursetable() {
        super();
    }

    /**
     * Access method for courseId.
     *
     * @return the current value of courseId
     */
    public String getCourseId() {
        return courseId;
    }

    /**
     * Setter method for courseId.
     *
     * @param aCourseId the new value for courseId
     */
    public void setCourseId(String aCourseId) {
        courseId = aCourseId;
    }

    /**
     * Access method for courseName.
     *
     * @return the current value of courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * Setter method for courseName.
     *
     * @param aCourseName the new value for courseName
     */
    public void setCourseName(String aCourseName) {
        courseName = aCourseName;
    }

    /**
     * Access method for updated.
     *
     * @return the current value of updated
     */
    public Timestamp getUpdated() {
        return updated;
    }

    /**
     * Setter method for updated.
     *
     * @param aUpdated the new value for updated
     */
    public void setUpdated(Timestamp aUpdated) {
        updated = aUpdated;
    }

    /**
     * Access method for baxterStudentxcoursetable.
     *
     * @return the current value of baxterStudentxcoursetable
     */
    public Set<BaxterStudentxcoursetable> getBaxterStudentxcoursetable() {
        return baxterStudentxcoursetable;
    }

    /**
     * Setter method for baxterStudentxcoursetable.
     *
     * @param aBaxterStudentxcoursetable the new value for baxterStudentxcoursetable
     */
    public void setBaxterStudentxcoursetable(Set<BaxterStudentxcoursetable> aBaxterStudentxcoursetable) {
        baxterStudentxcoursetable = aBaxterStudentxcoursetable;
    }

    /**
     * Access method for baxterTeacherxcoursetable.
     *
     * @return the current value of baxterTeacherxcoursetable
     */
    public Set<BaxterTeacherxcoursetable> getBaxterTeacherxcoursetable() {
        return baxterTeacherxcoursetable;
    }

    /**
     * Setter method for baxterTeacherxcoursetable.
     *
     * @param aBaxterTeacherxcoursetable the new value for baxterTeacherxcoursetable
     */
    public void setBaxterTeacherxcoursetable(Set<BaxterTeacherxcoursetable> aBaxterTeacherxcoursetable) {
        baxterTeacherxcoursetable = aBaxterTeacherxcoursetable;
    }

    /**
     * Compares the key for this instance with another BaxterCoursetable.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BaxterCoursetable and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BaxterCoursetable)) {
            return false;
        }
        BaxterCoursetable that = (BaxterCoursetable) other;
        Object myCourseId = this.getCourseId();
        Object yourCourseId = that.getCourseId();
        if (myCourseId==null ? yourCourseId!=null : !myCourseId.equals(yourCourseId)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BaxterCoursetable.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BaxterCoursetable)) return false;
        return this.equalKeys(other) && ((BaxterCoursetable)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCourseId() == null) {
            i = 0;
        } else {
            i = getCourseId().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BaxterCoursetable |");
        sb.append(" courseId=").append(getCourseId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("courseId", getCourseId());
        return ret;
    }

}
