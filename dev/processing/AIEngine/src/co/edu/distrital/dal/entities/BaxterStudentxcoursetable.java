// Generated with g9.

package co.edu.distrital.dal.entities;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="baxter_studentxcoursetable")
public class BaxterStudentxcoursetable implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(nullable=false, length=1)
    private boolean auth;
    @ManyToOne(optional=false)
    @JoinColumn(name="course_id", nullable=false)
    private BaxterCoursetable baxterCoursetable;
    @ManyToOne(optional=false)
    @JoinColumn(name="student_id", nullable=false)
    private BaxterUsertable baxterUsertable;
    @OneToMany(mappedBy="baxterStudentxcoursetable")
    private Set<BaxterUserperformancetable> baxterUserperformancetable;

    /** Default constructor. */
    public BaxterStudentxcoursetable() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for auth.
     *
     * @return true if and only if auth is currently true
     */
    public boolean getAuth() {
        return auth;
    }

    /**
     * Setter method for auth.
     *
     * @param aAuth the new value for auth
     */
    public void setAuth(boolean aAuth) {
        auth = aAuth;
    }

    /**
     * Access method for baxterCoursetable.
     *
     * @return the current value of baxterCoursetable
     */
    public BaxterCoursetable getBaxterCoursetable() {
        return baxterCoursetable;
    }

    /**
     * Setter method for baxterCoursetable.
     *
     * @param aBaxterCoursetable the new value for baxterCoursetable
     */
    public void setBaxterCoursetable(BaxterCoursetable aBaxterCoursetable) {
        baxterCoursetable = aBaxterCoursetable;
    }

    /**
     * Access method for baxterUsertable.
     *
     * @return the current value of baxterUsertable
     */
    public BaxterUsertable getBaxterUsertable() {
        return baxterUsertable;
    }

    /**
     * Setter method for baxterUsertable.
     *
     * @param aBaxterUsertable the new value for baxterUsertable
     */
    public void setBaxterUsertable(BaxterUsertable aBaxterUsertable) {
        baxterUsertable = aBaxterUsertable;
    }

    /**
     * Access method for baxterUserperformancetable.
     *
     * @return the current value of baxterUserperformancetable
     */
    public Set<BaxterUserperformancetable> getBaxterUserperformancetable() {
        return baxterUserperformancetable;
    }

    /**
     * Setter method for baxterUserperformancetable.
     *
     * @param aBaxterUserperformancetable the new value for baxterUserperformancetable
     */
    public void setBaxterUserperformancetable(Set<BaxterUserperformancetable> aBaxterUserperformancetable) {
        baxterUserperformancetable = aBaxterUserperformancetable;
    }

    /**
     * Compares the key for this instance with another BaxterStudentxcoursetable.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BaxterStudentxcoursetable and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BaxterStudentxcoursetable)) {
            return false;
        }
        BaxterStudentxcoursetable that = (BaxterStudentxcoursetable) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BaxterStudentxcoursetable.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BaxterStudentxcoursetable)) return false;
        return this.equalKeys(other) && ((BaxterStudentxcoursetable)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BaxterStudentxcoursetable |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
