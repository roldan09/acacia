/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.distrital.dal.model;

/**
 *
 * @author fjroldan
 */
public class ClusterFrecuency {

    private double concentrated;
    private double anxious;
    private double boring;
    private double undetermined;

    public ClusterFrecuency() {
        concentrated = 0;
        anxious = 0;
        boring = 0;
        undetermined = 0;
    }

    public void concentratedUP() {
        concentrated++;
    }

    public void anxiousUP() {
        anxious++;
    }

    public void boringUP() {
        boring++;
    }

    public void undeterminedUP() {
        undetermined++;
    }

    public double getConcentrated() {
        return concentrated;
    }

    public double getAnxious() {
        return anxious;
    }

    public double getBoring() {
        return boring;
    }

    public double getUndetermined() {
        return undetermined;
    }

}
