// Generated with g9.

package co.edu.distrital.dal.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

@Entity(name="baxter_expertbindtable")
public class BaxterExpertbindtable implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(nullable=false)
    private Timestamp updated;
    @ManyToOne(optional=false)
    @JoinColumn(name="expert_id", nullable=false)
    private BaxterExpert baxterExpert;
    @ManyToOne(optional=false)
    @JoinColumn(name="row_id", nullable=false)
    private BaxterBaxtertable baxterBaxtertable;

    /** Default constructor. */
    public BaxterExpertbindtable() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for updated.
     *
     * @return the current value of updated
     */
    public Timestamp getUpdated() {
        return updated;
    }

    /**
     * Setter method for updated.
     *
     * @param aUpdated the new value for updated
     */
    public void setUpdated(Timestamp aUpdated) {
        updated = aUpdated;
    }

    /**
     * Access method for baxterExpert.
     *
     * @return the current value of baxterExpert
     */
    public BaxterExpert getBaxterExpert() {
        return baxterExpert;
    }

    /**
     * Setter method for baxterExpert.
     *
     * @param aBaxterExpert the new value for baxterExpert
     */
    public void setBaxterExpert(BaxterExpert aBaxterExpert) {
        baxterExpert = aBaxterExpert;
    }

    /**
     * Access method for baxterBaxtertable.
     *
     * @return the current value of baxterBaxtertable
     */
    public BaxterBaxtertable getBaxterBaxtertable() {
        return baxterBaxtertable;
    }

    /**
     * Setter method for baxterBaxtertable.
     *
     * @param aBaxterBaxtertable the new value for baxterBaxtertable
     */
    public void setBaxterBaxtertable(BaxterBaxtertable aBaxterBaxtertable) {
        baxterBaxtertable = aBaxterBaxtertable;
    }

    /**
     * Compares the key for this instance with another BaxterExpertbindtable.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BaxterExpertbindtable and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BaxterExpertbindtable)) {
            return false;
        }
        BaxterExpertbindtable that = (BaxterExpertbindtable) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BaxterExpertbindtable.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BaxterExpertbindtable)) return false;
        return this.equalKeys(other) && ((BaxterExpertbindtable)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BaxterExpertbindtable |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
