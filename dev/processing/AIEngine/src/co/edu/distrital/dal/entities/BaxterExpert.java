// Generated with g9.

package co.edu.distrital.dal.entities;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

@Entity(name="baxter_expert")
public class BaxterExpert implements Serializable {

    /** Primary key. */
    protected static final String PK = "id";

    /**
     * The optimistic lock. Available via standard bean get/set operations.
     */
    @Version
    @Column(name="LOCK_FLAG")
    private Integer lockFlag;

    /**
     * Access method for the lockFlag property.
     *
     * @return the current value of the lockFlag property
     */
    public Integer getLockFlag() {
        return lockFlag;
    }

    /**
     * Sets the value of the lockFlag property.
     *
     * @param aLockFlag the new value of the lockFlag property
     */
    public void setLockFlag(Integer aLockFlag) {
        lockFlag = aLockFlag;
    }

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(nullable=false, length=1)
    private String gender;
    @Column(nullable=false, length=5)
    private String age;
    @Column(nullable=false, length=1)
    private String profession;
    @Column(nullable=false, length=50)
    private String scholarship;
    @Column(nullable=false)
    private Timestamp updated;
    @OneToMany(mappedBy="baxterExpert")
    private Set<BaxterExpertbindtable> baxterExpertbindtable;

    /** Default constructor. */
    public BaxterExpert() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for gender.
     *
     * @return the current value of gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Setter method for gender.
     *
     * @param aGender the new value for gender
     */
    public void setGender(String aGender) {
        gender = aGender;
    }

    /**
     * Access method for age.
     *
     * @return the current value of age
     */
    public String getAge() {
        return age;
    }

    /**
     * Setter method for age.
     *
     * @param aAge the new value for age
     */
    public void setAge(String aAge) {
        age = aAge;
    }

    /**
     * Access method for profession.
     *
     * @return the current value of profession
     */
    public String getProfession() {
        return profession;
    }

    /**
     * Setter method for profession.
     *
     * @param aProfession the new value for profession
     */
    public void setProfession(String aProfession) {
        profession = aProfession;
    }

    /**
     * Access method for scholarship.
     *
     * @return the current value of scholarship
     */
    public String getScholarship() {
        return scholarship;
    }

    /**
     * Setter method for scholarship.
     *
     * @param aScholarship the new value for scholarship
     */
    public void setScholarship(String aScholarship) {
        scholarship = aScholarship;
    }

    /**
     * Access method for updated.
     *
     * @return the current value of updated
     */
    public Timestamp getUpdated() {
        return updated;
    }

    /**
     * Setter method for updated.
     *
     * @param aUpdated the new value for updated
     */
    public void setUpdated(Timestamp aUpdated) {
        updated = aUpdated;
    }

    /**
     * Access method for baxterExpertbindtable.
     *
     * @return the current value of baxterExpertbindtable
     */
    public Set<BaxterExpertbindtable> getBaxterExpertbindtable() {
        return baxterExpertbindtable;
    }

    /**
     * Setter method for baxterExpertbindtable.
     *
     * @param aBaxterExpertbindtable the new value for baxterExpertbindtable
     */
    public void setBaxterExpertbindtable(Set<BaxterExpertbindtable> aBaxterExpertbindtable) {
        baxterExpertbindtable = aBaxterExpertbindtable;
    }

    /**
     * Compares the key for this instance with another BaxterExpert.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class BaxterExpert and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof BaxterExpert)) {
            return false;
        }
        BaxterExpert that = (BaxterExpert) other;
        if (this.getId() != that.getId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another BaxterExpert.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof BaxterExpert)) return false;
        return this.equalKeys(other) && ((BaxterExpert)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[BaxterExpert |");
        sb.append(" id=").append(getId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("id", Integer.valueOf(getId()));
        return ret;
    }

}
