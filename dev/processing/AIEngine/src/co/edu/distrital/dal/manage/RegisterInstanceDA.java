/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.distrital.dal.manage;

import co.edu.distrital.dal.entities.BaxterBaxtertable;
import co.edu.distrital.dal.entities.BaxterCursorperformancetable;
import co.edu.distrital.dal.entities.BaxterUsertable;
import java.sql.Timestamp;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author fjroldan
 */
public class RegisterInstanceDA {

    private EntityManager entityManager;
    private EntityManagerFactory factory;

    public RegisterInstanceDA() {
        factory = Persistence.createEntityManagerFactory("ProcessAPIPU");
        entityManager = factory.createEntityManager();
    }

    public boolean process(String id, int cluster) {
        String sql;
        Query query;
        int frecuency;
        Integer idexc;
        double concentrated, anxious, boring;
        concentrated = 0.0;
        boring = 0.0;
        anxious = 0.0;        
        // <User_ID, Course_ID, Entity_ID>
        String[] splitID = id.split("-");
        BaxterCursorperformancetable bcptObj = null;
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        
        // Check
        if(splitID.length != 3)
            return false;

        // Get user ID
        String userID = splitID[0] + "-" + splitID[1];
        
        // Begin transaction
        entityManager.getTransaction().begin();

        // Do query
        sql = "SELECT b from baxter_baxtertable b where b.id = " + splitID[2];
        query = entityManager.createQuery(sql);
        BaxterBaxtertable bb = (BaxterBaxtertable) query.getSingleResult();
            
        // Do query
        sql = "select b.id from baxter_studentxcoursetable b where course_id = '" + splitID[0] + "' and student_id = '" + userID + "'";
        query = entityManager.createQuery(sql);
        if (!query.getResultList().isEmpty()) {
            idexc = (Integer) query.getSingleResult();
            if(cluster == 0 || cluster == 1) {
                concentrated = 1.0;
            } else {
                boring = 1.0;
            }
            entityManager.createNativeQuery("INSERT INTO baxter_userperformancetable (session, concentrated, anxious, boring, studentxcourse_id) VALUES (current_timestamp,?,?,?,?)")
                    .setParameter(1, concentrated)
                    .setParameter(2, anxious)
                    .setParameter(3, boring)
                    .setParameter(4, idexc)
                    .executeUpdate();
            
            // Register log
            sql = "select b from baxter_cursorperformancetable b ";
            query = entityManager.createQuery(sql);
            if (!query.getResultList().isEmpty()) {
                List<BaxterCursorperformancetable> bcptList = query.getResultList();
                for (BaxterCursorperformancetable  bcpt : bcptList) {
                    if (bcpt.getBaxterUsertable().getUserId().equals(userID)) {
                        bcptObj = bcpt; 
                    }
                }
            }
            if (bcptObj == null) {
                sql = "SELECT b from baxter_usertable b ";
                query = entityManager.createQuery(sql);
                if (!query.getResultList().isEmpty()) {                    
                    List<BaxterUsertable> userList = query.getResultList();
                    for (BaxterUsertable u : userList) {
                        if (u.getUserId().equals(userID)) {
                            bcptObj = new BaxterCursorperformancetable();
                            bcptObj.setAlias(u.getUserName());
                            bcptObj.setBaxterUsertable(u);
                            bcptObj.setCourse(splitID[0]);
                            bcptObj.setFrequency("0");
                            bcptObj.setSession(timestamp);
                            bcptObj.setStartDate("Year: " + timestamp.getYear());
                            bcptObj.setState("Active");
                            entityManager.persist(bcptObj);
                        }                        
                    }
                }
            } else {
                frecuency = Integer.parseInt(bcptObj.getFrequency());
                frecuency++;
                bcptObj.setFrequency("" + frecuency);
                entityManager.persist(bcptObj);
            }
            
        }

        // Persist 
        bb.setAuth(true);
        entityManager.persist(bb);
            
        // End transaction
        entityManager.getTransaction().commit();

        return true;
    }

    public void finalice() {
        entityManager.close();
        factory.close();
    }
}
