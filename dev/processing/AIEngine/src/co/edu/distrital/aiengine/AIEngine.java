/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.distrital.aiengine;

import co.edu.distrital.business.dataprocess.DataProcessDirector;
import co.edu.distrital.business.facade.APIFacade;
import co.edu.distrital.business.machine.Engine;
import co.edu.distrital.util.Trace;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.Timestamp;

/**
 *
 * @author fjroldan
 */
public class AIEngine {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Trace trace = Trace.getInstance();
        trace.info("");
        trace.info("");
        trace.info("=========================================================");
        trace.info("Starting AIEngine ...");
        APIFacade facade = new APIFacade();
        facade.startWebSocketServer();
        trace.info("=========================================================");
        trace.info("AIEngine start [OK].");
        // Start periodic process
        AIEngine ai = new AIEngine();
        ai.process();
    }

    private void delay(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            Logger.getLogger(AIEngine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void process() {
        String data;
        boolean checking;
        Timestamp timestamp;
        DataProcessDirector dpd;
        Engine engine = new Engine();
        Trace trace = Trace.getInstance();

        // Delay
        delay(60000);

        // Run
        while (true) {
            checking = true;
            dpd = new DataProcessDirector();
            checking &= dpd.loadData();
            checking &= dpd.extractData();
            dpd.finalice();
            data = "Process clasify [ERROR]";

            trace.info("");
            trace.info("");
            trace.info("");
            trace.info(":[Start Process] ==================================================");
            timestamp = new Timestamp(System.currentTimeMillis());
            trace.info(" time: " + timestamp.toString());

            if (checking) {
                Object intances = dpd.processClasify();
                checking &= engine.process(intances);
                data = checking ? "Process clasify [Ok]" : "Process clasify [ERROR]";
            }
            timestamp = new Timestamp(System.currentTimeMillis());
            trace.info(" time: " + timestamp.toString() + " result: " + data);
            trace.info(":[End Process] ==================================================");
            trace.info("");
            trace.info("");
            trace.info("");
            trace.info(":[Period 3H]...");
            
            // Period 3h
            delay(10800000);            
        }
    }

}
