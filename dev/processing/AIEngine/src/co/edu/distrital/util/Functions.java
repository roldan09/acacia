/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.distrital.util;

/**
 *
 * @author fjroldan
 */
public class Functions {
    
    public static double normalizeAngle(String value) {
        if (value == null) return 0.0;         
        double val = Math.abs(Double.parseDouble(value.trim()));
        return (0.005 * val) + 0.5;
    }
    
    public static double normalizeEmotion(String value) {
        if (value == null) return 0.0;
        double val = Math.abs(Double.parseDouble(value.trim()));
        return (val / 100);
    }
    
    public static double normalizeExpression(String value) {
        if (value == null) return 0.0;
        double val = Math.abs(Double.parseDouble(value.trim()));
        return (val / 100);
    }
        
}
