/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.distrital.util;

/**
 * Class that contains util methods for process data string.
 * 
 * @author fjroldan
 */
public class Text {
    
    public static void print(String tag, String[] str) {
        System.out.println();
        System.out.println("Vector " + tag + " { ");
        for (int i = 0; i < str.length; i++) {
            System.out.print(str[i] + ", ");
        }
        System.out.println("}");
    }
    
    public static void print(String tag, double[] str) {
        System.out.println();
        System.out.println("Vector " + tag + " { ");
        for (int i = 0; i < str.length; i++) {
            System.out.print(str[i] + ", ");
        }
        System.out.println("}");
    }
}
