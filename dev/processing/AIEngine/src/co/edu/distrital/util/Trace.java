/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.distrital.util;

/**
 *
 * @author fjroldan
 */
public class Trace {
    
    private static final Trace INSTANCE = new Trace();

    private String pattern;
    
    private Trace() {
        pattern = "[ENGINE]:";
    }
    
    public void info(String msg) {
        System.out.println(pattern + "[INFO]: " + msg);
    }
    
    public void warn(String msg) {
        System.out.println(pattern + "[WARN]: " + msg);
    }
    
    public void erro(String msg, Exception e) {
        System.out.println(pattern + "[ERRO]: " + msg + " :::: " + e.getMessage());
        e.printStackTrace();
    }

    public static Trace getInstance() {
        return INSTANCE;
    }
    
}
