/*
 * Universidad Distrital Francias Jose de caldas
 * @Todos los derechos.
 */
package co.edu.distrital.business.facade;

import co.edu.distrital.business.dataprocess.DataProcessDirector;
import co.edu.distrital.business.machine.Engine;
import co.edu.distrital.util.Trace;
import java.util.ArrayList;
import java.util.List;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.json.JSONObject;

@ServerEndpoint(value = "/ai")
public class FacadeServerEndpoint {

    @OnOpen
    public void onOpen(Session session) {
        Trace.getInstance().info("Connected ... " + session.getId());        
    }

    @OnMessage
    public String onMessage(String message, Session session) {        
        String data, msg;
        boolean checking;
        DataProcessDirector dpd;
        Engine engine = new Engine();
        Trace trace = Trace.getInstance();        
        JSONObject response = new JSONObject();
        JSONObject cmd = new JSONObject(message);
        String command = (String)cmd.get("command");
        trace.info("Receive " + command + " command.");
        switch (command) {
            case "PROCESS_INPUT_DATA":
                checking = true;
                dpd = new DataProcessDirector();
                checking &= dpd.loadData();
                checking &= dpd.extractData();
                msg = dpd.processTraining();
                checking &= (msg != null) ? true : false;                  
                dpd.finalice();
                data = msg;
                response.append("command", "PROCESS_RESPONSE");
                response.append("data", data);
                message = response.toString();
                break;
            case "GENERATE_LEARNING_MODEL":
                response.append("command", "RESPONSE");                
                engine.genarateClustersKMeans((String)cmd.get("data"));
                message = response.toString();
                break;
            case "VALIDATE_LEARNING_MODEL":
                response.append("command", "RESPONSE");
                engine.validateModel((String)cmd.get("data"));
                message = response.toString();
                break;
            case "CLASSIFY_INSTANCE":                
                data = "Process clasify [ERROR]";
                response.append("command", "RESPONSE");
                checking = true;
                dpd = new DataProcessDirector();
                checking &= dpd.loadData();
                checking &= dpd.extractData();
                dpd.finalice();
                if (checking) {                    
                    Object intances = dpd.processClasify();
                    checking &= engine.process(intances);
                    data = checking ? "Process clasify [Ok]" : "Process clasify [ERROR]";
                }                     
                response.append("data", data);
                message = response.toString();
                break;
        }        
        trace.info("Response message: " + message);
        return message;
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        Trace.getInstance().info(String.format("Session %s closed because of %s", session.getId(), closeReason));
    }

    public String[] mySplit(String msg, char pattern) {
        String carrier = "";
        List<String> rtn = new ArrayList<>();
        for (int i = 0; i < msg.length(); i++) {
            if (msg.charAt(i) == pattern) {
                rtn.add(carrier);
                carrier = "";
            } else {
                carrier += msg.charAt(i);
            }
        }
        if (!rtn.isEmpty() && !carrier.isEmpty()) {
            rtn.add(carrier);
            String[] rtnArr = new String[rtn.size()];
            for (int i = 0; i < rtn.size(); i++) {
                rtnArr[i] = rtn.get(i);
            }
            return rtnArr;
        }
        return null;
    }

}
