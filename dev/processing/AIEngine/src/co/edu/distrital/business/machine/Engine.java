package co.edu.distrital.business.machine;

import co.edu.distrital.dal.manage.RegisterInstanceDA;
import co.edu.distrital.dal.model.HistogramData;
import co.edu.distrital.dal.model.Tuple;
import co.edu.distrital.util.Trace;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;
import weka.core.Debug;
import weka.core.Instance;
import weka.core.converters.ArffLoader.ArffReader;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

/**
 *
 * @author fjroldan
 */
public class Engine {

    /**
     * Algoritm kmeans.
     */
    private SimpleKMeans kMeans;

    /**
     * Count de clouster.
     */
    private int clusterCount;

    /**
     * Constructor by default.
     */
    public Engine() {
    }

    /**
     * Generate the clousters from de K-MEANS.
     *
     * @param file type to porcess
     */
    public void genarateClustersKMeans(String file) {
        Trace trace = Trace.getInstance();
        try {
            trace.info("============================================================");
            trace.info(" START OF KMEANS GENERATION");
            trace.info("============================================================");
            trace.info("");
            // Create an instance of algoritm.
            kMeans = new SimpleKMeans();
            kMeans.setNumClusters(4);
            // Get training data.
            Instances data = readDada(file + "-training.arff");            
            // Make the Cluster.
            kMeans.buildClusterer(data);
            trace.info("------------------------------------------------------------");
            trace.info(" CLOUSTER");
            trace.info("------------------------------------------------------------");
            trace.info("");
            // Log centroides.
            System.out.println("CENTROIDES KMEANS-------------------------------------------");
            Instances centroids = kMeans.getClusterCentroids();
            clusterCount = centroids.numInstances();
            for (int i = 0; i < centroids.numInstances(); i++) {
                trace.info("Centroid " + i + 1 + ": " + centroids.instance(i));
            }
            System.out.println("INSTANCES KMEANS---------------------------------------------");
            // Classify the instans into clouster.
            for (int i = 0; i < data.size(); i++) {
                trace.info(data.get(i) + " is in cluster " + kMeans.clusterInstance((Instance) data.get(i)) + 1);
            }
            // Save the modelo.
            Debug.saveToFile("assets/models/KMEANSModel", kMeans);
        } catch (Exception ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Generate the meaning of clusters.
     *
     * @param file category to process
     */
    public void validateModel(String file) {
        int cluster;
        int index = 0;
        Trace trace = Trace.getInstance();
        HistogramData histo = new HistogramData();
        try {
            trace.info("============================================================");
            trace.info(" START OF MEANING MODEL");
            trace.info("============================================================");
            trace.info("");
            // Read the tag data.
            ArrayList<String> tagList = readTags(file + "-tags.obj");
            // Get meaning data.
            Instances data = readDada(file + "-meaning.arff");            
            // Get model
            kMeans = (SimpleKMeans) Debug.loadFromFile("assets/models/KMEANSModel");
            // Make porcess
            for (Instance inst : data) {
                cluster = kMeans.clusterInstance(inst);
                trace.info("Instance: " + index + " into cluster: " + cluster + " with tag: " + tagList.get(index));
                histo.update(cluster, tagList.get(index));
                index++;
            }
            // Print reuslts
            histo.printResults();
        } catch (Exception ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Make the classify process.
     *
     * @param instanceSet instances to process
     * @return result of the process
     */
    public boolean process(Object instanceSet) {
        int cluster;
        int index = 0;        
        Trace trace = Trace.getInstance();
        RegisterInstanceDA ria = new RegisterInstanceDA();
        try {
            trace.info("============================================================");
            trace.info(" START OF CLASSIFY");
            trace.info("============================================================");
            trace.info("");
            // Get meaning data.
            ArrayList<Tuple> data = (ArrayList<Tuple>) instanceSet;            
            // Get model
            kMeans = (SimpleKMeans) Debug.loadFromFile("/usr/src/app/AIEngine/assets/models/KMEANSModel");
            // Make porcess
            for (Tuple tuple : data) {
                try {
                cluster = kMeans.clusterInstance(tuple.getIntance());
                trace.info("Instance: " + tuple.getId() + " into cluster: " + cluster);
                ria.process(tuple.getId(), cluster);
                index++;
                } catch (Exception e) {
                    trace.erro("Instance error: ", e);
                } 
            }
        } catch (Exception ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }

    /**
     * Read data instances
     *
     * @param file to read
     * @return null or object
     */
    private Instances readDada(String file) {
        Instances data = null;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("assets/data/" + file));
            ArffReader arff = new ArffReader(reader, 1000);
            data = arff.getStructure();
            Instance inst;
            while ((inst = arff.readInstance(data)) != null) {
                data.add(inst);
            }
            return data;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return data;
    }

    /**
     * Read tag file.
     *
     * @param file to read
     * @return null if not exist or object
     */
    private ArrayList<String> readTags(String file) {
        try {
            FileInputStream fileIn = new FileInputStream("assets/data/" + file);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            Object obj = objectIn.readObject();
            objectIn.close();
            return (ArrayList<String>) obj;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public int getClusterCount() {
        return clusterCount;
    }

    public void setClusterCount(int clusterCount) {
        this.clusterCount = clusterCount;
    }

    public SimpleKMeans getkMeans() {
        return kMeans;
    }

    public void setkMeans(SimpleKMeans kMeans) {
        this.kMeans = kMeans;
    }

}
