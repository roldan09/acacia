/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.distrital.business.dataprocess;

import co.edu.distrital.dal.manage.TableETL;
import co.edu.distrital.dal.model.Tuple;
import co.edu.distrital.exception.AIException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import weka.core.Instances;

/**
 *
 * @author fjroldan
 */
public class DataProcessDirector {
    
    private TableETL tableETL;
    private EntityManager entityManager;
    private EntityManagerFactory factory;

    public DataProcessDirector() {        
        factory = Persistence.createEntityManagerFactory("ProcessAPIPU");
        entityManager = factory.createEntityManager();
        tableETL = new TableETL(entityManager);
    }
    
    public boolean loadData() {
        try {
            tableETL.loadData();
            return true;
        } catch (AIException ex) {
            Logger.getLogger(DataProcessDirector.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public boolean extractData() {
        try {
            tableETL.extractData();
            return true;
        } catch (AIException ex) {
            Logger.getLogger(DataProcessDirector.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public String processTraining() {
        try {
            return tableETL.process(true);
        } catch (AIException ex) {
            Logger.getLogger(DataProcessDirector.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public ArrayList<Tuple> processClasify() {
        try {
            tableETL.process(false);
            return tableETL.getData();
        } catch (AIException ex) {
            Logger.getLogger(DataProcessDirector.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public void finalice() {
        entityManager.close();
        factory.close();
    }
}
