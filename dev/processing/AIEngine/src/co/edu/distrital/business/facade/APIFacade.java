/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.distrital.business.facade;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.Session;
import org.glassfish.tyrus.server.Server;
import org.json.JSONObject;

/**
 *
 * @author usuario
 */
public class APIFacade {

    private Session session;

    public void startWebSocketServer() {
        Server server = new Server("localhost", 8025, "/api", null, FacadeServerEndpoint.class);
        Executors.newCachedThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                try {
                    server.start();
                    Thread.currentThread().join();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                } finally {
                    server.stop();
                }
            }
        });
    }

    public void sendMessaggeToGUI(String msg) {
        try {
            JSONObject response = new JSONObject();
            response.append("command", "MSG");
            response.append("info", "Fin de escena. Presiona el botón: Continuar.");
            session.getBasicRemote().sendText(response.toString());
        } catch (IOException ex) {
            Logger.getLogger(APIFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

}
