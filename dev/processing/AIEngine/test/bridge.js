var wekaData = '2019-06-09-17-15-58';
var webSocket = WebSocketTest();

var iData = document.getElementById("data-input-it");
iData.value = wekaData; 
                    
function sendCommand(command) {
    sendCommand(command, '');
}

function sendCommand(command, data) {
    webSocket.send(JSON.stringify({command: command, data: data}));
    trace('Sent command: ' + command + ' with the data: ' + data + '.');
}

function WebSocketTest() {
    var ws = null;

    if ("WebSocket" in window) {
        ws = new WebSocket("ws://localhost:8025/api/ai");

        ws.onopen = function () {
            //ws.send(JSON.stringify({command: 'INIT', data: ''}));
            trace('Socket init.');
        };

        ws.onmessage = function (evt) {
            var msg = JSON.parse(evt.data);
            trace('Response data: ' + msg.data);
            switch (msg.command[0]) {
                case 'RESPONSE':
                    trace('OK');
                    break;
                case 'PROCESS_RESPONSE':
                    wekaData = msg.data;                    
                    trace('Weka file: ' + wekaData);
                    iData.value = wekaData;                    
                    break;
                case 'ERROR':
                    $("#alertMsg").empty();
                    $("#alertMsg").append('<p>Se presentó un error de comunicación, intenta de nuevo.</p>');
                    break;                
                case 'MSG':
                    trace(message);                    
                    break;
            }
        };

        ws.onclose = function () {
            trace('Socket close.');
        };

        ws.onbeforeunload = function (event) {
            socket.close();            
        };

        ws.onerror = function (err) {
            $("#alertMsg").empty();
            $("#alertMsg").append('<p>Communication error.</p>');
            trace('Comuncation error.');
        };

        this.remote = ws;
    } else {
        trace('Brouser doesnt support.');
        $("#alertMsg").empty();
        $("#alertMsg").append('<p>The tester doesnt support brouser.</p>');
        $('#alertMsgBox').modal('show');
    }

    return ws;
}

function errorMessage(msg) {
    var html = "<div class=\" alert alert-error\">";
    html += "    <p  class=\" msgError\"  >" + msg + "</p>";
    html += "</div>";
    return html;
}

function trace(message) {
    console.log('[INFO]: ', message)
    $(".console-app").append('<p>$> ' + message + ' </p>');
}

function getWekaData() {
    var it = document.getElementById("data-input-it");
    return it.value;
}